# RLAlgo.jl

# Roadmap

1. Add option to use SimpleChains.jl for functim approximation. We could probably add a type parameter to algos that determined the type of function approximation, Flux or SimpleChains

# ToDos:
- [ ] Figure out why we get NaNs with TruncatedPolicy with half-bounded
	distributions
- [ ] Don't have functions returning abstract types
- [ ] Don't have functions with AbstractType args

