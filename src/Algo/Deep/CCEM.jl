@enum UpdateStrategy begin
    None            # Do not update the proposal policy
    ActorStrategy   # Update the proposal policy as a "regular" actor policy
    TargetStrategy  # Update the proposal policy as a target network
end

"""
TODO: Update this documentation!!

# Description
CCEM implements an actor-critic algorithm using the Conditional Cross-Entropy Method (CCEM)
for policy improvement, from [1].

## Updates

This section discusses which update targets are used to update the actor and critic, as well
as some implementation details on how these updates are performed.

### Actor Policy π

The actor policy, denoted as π, always uses a CCEM update for the policy improvement step of
approximate policy iteration (API).

For entropy regularization for the actor, this implementation samples actions and estimates
the entropy as -𝔼[ln(π(A∣S) ∇ln(π(A∣S)].

### Proposal Policy π̃

The proposal policy, denoted as π̃, is updated using one of three different methods:
    1. When π̃_update_strategy == ActorStrategy, then a CCEM update is used similar
        to the actor policy.
    2.  When π̃_update_strategy == None and with discrete actions, the modified CCEM update
        outlined in [1] can be used, where only the maximally valued action is used in the
        CCEM update. The `None` method cannot be used in the continuous action setting.
    3. When π̃_update_strategy == TargetStrategy, a target network method, where the
        parameters of the proposal policy are slowly updated toward those of the actor
        policy: ϕ̃ <- (1 - τ) ϕ̃ + τ ϕ, where ϕ are the parameters of the actor policy π,
        and ϕ̃ are the parameters of the proposal policy π̃.

Using update method (1) for continuous actions and (2) for discrete actions, we get the
GreedyAC algorithm from [1]. The third update method (3) matches some of the theory in [1]
which proves that the CCEM tracks the expected CEM optimizer across states.

For entropy regularization for the proposal policy, this implementation samples actions
and estimates the entropy as -𝔼[ln(π(A∣S) ∇ln(π(A∣S)].

### Critic Update

To learn a critic, this algorithm uses a Sarsa update.

When learning soft Q functions, this class uses the following update target, where the
temperature τ is treated as an entropy scale as in [j] (rather than a reward scale as can
be found in [3, 4]):
    q(s, a) = 𝔼[r + γ(q(s', a') - τ ln(π(a|s))]
and otherwise uses the "regular" Sarsa update target:
    q(s, a) = 𝔼[r + γq(s', a')]

# References
[1] S. Neumann, Lim, S., Joseph, A., Pan, Y., White, A., White, M. Greedy
Actor-Critic: A New Conditional Cross-Entropy Method for Policy
Improvement. 2022.

[2] Greedification Operators for Policy Optimization: Investigating Forward
and Reverse KL Divergences. Chan, A., Silva H., Lim, S., Kozuno, T.,
Mahmood, A. R., White, M. 2021.

[3] Soft Actor-Critic: Off-Policy Maximum Entropy Deep Reinforcement
Learning with a Stochastic Actor. Haarnoja, T., Zhou, A., Abbeel, P.,
Levine, S. International Conference on Machine Learning. 2018.

[4] Soft Actor-Critic: Algorithms and Applications. Haarnoja, T.,
Zhou, A., Hartikainen, K., Tucker, G., Ha, S., Tan, J., Kumar, V.,
Zhu, H., Gupta, A., Abbeel, P., Levine, S. In preparation. 2019.
"""
mutable struct CCEM{
        F<:AbstractFloat,                       # Floating point type
        Π<:AbstractPolicy,                      # Actor policy type
        ΠO,                                     # Actor optimizer
        Π̃,                                      # Proposal policy type
        Π̃O,                                     # Proposal optimizer
        V<:AbstractValueFunction,               # Value function type
        VO,                                     # Value function optimizer
        E<:AbstractEnvironment,                 # Environment type
        ER<:AbstractReplay,                     # Experience replay
        R<:AbstractRNG,                         # RNG
} <: AbstractAlgo
    _env::E
    _rng::R

    _π::Π
    _π_opt::ΠO
    _π̃::Π̃
    _π̃_opt::Π̃O
    _π̃_update_type::UpdateStrategy

    _critic::V
    _critic_opt::VO
    _critic_target::V

    _soft_q::Bool

    _replay::ER
    _batch_size::Int

    _τ_π::F # Entropy scale for actor
    _τ_π̃::F # Entropy scale for proposal
    _π_entropy_actions::Int
    _π̃_entropy_actions::Int

    _p_critic::F
    _p_π̃::F
    _target_refresh_rate::Int
    _current_update::Int

    _steps_before_learning::Int # Number of timesteps to pass before learning
    _current_step::Int

    # CCEM update
    _num_samples::Int
    _ρ_π::F
    _ρ_π̃::F

    _is_training::Bool

    function CCEM{F}(
        env::E,
        rng::R,
        π::Π,
        π_opt::ΠO,
        π̃::Π̃,
        π̃_opt::Π̃O,
        π̃_update_type::UpdateStrategy,
        critic::V,
        critic_opt::VO,
        critic_target::V,
        replay::ER,
        batch_size,
        τ_π,
        τ_π̃,
        π_entropy_actions,
        π̃_entropy_actions,
        p_critic,
        p_π̃,
        target_refresh_rate,
        steps_before_learning,
        num_samples,
        ρ_π,
        ρ_π̃,
    ) where{
        F<:AbstractFloat,
        Π<:AbstractPolicy,
        ΠO,
        Π̃,
        Π̃O,
        V<:AbstractValueFunction,
        VO,
        E<:AbstractEnvironment,
        ER,
        R<:AbstractRNG,
    }
        # Ensure the policy and action space match
        if discrete(π) && continuous(action_space(env))
            error("cannot use discrete policy for continuous action space")
        elseif continuous(π) && discrete(action_space(env))
            error("cannot use continuous policy for discrete action space")
        end

        if π̃_update_type == TargetStrategy
            if ρ_π̃ != 0
                error(
                    "when using target-like π̃ updating, ρ_π̃ == 0 is required"
                )
            elseif τ_π̃ != 0
                error(
                    "when using target-like π̃ updating, τ_π̃ == 0 is required"
                )
            end

            if π̃_entropy_actions != 0
                error(
                    "when using target-like π̃ updating, π̃_entropy_actions == 0 is requried"
                )
            end

            if p_π̃ <= 0 || p_π̃ >= 1
                error("expected p_π̃ ∈ (0, 1) but got $p_π̃")
            end

        elseif π̃_update_type == None
            if ρ_π̃ != 0
                error("expected ρ_π̃ to be 0 but got $ρ_π̃")
            end
            if τ_π̃ != 0
                error("expected τ_π̃ to be 0 but got $τ_π̃")
            end
            if p_π̃ != 0
                error("expected p_π̃ to be 0 but got $p_π̃")
            end
            if π̃_entropy_actions != 0
                error("expected π̃_entropy_actions to be 0 but got $π̃_entropy_actions")
            end

        elseif π̃_update_type == ActorStrategy
            if p_π̃ != 0
                error(
                    "when using actor-like proposal updated, p_π̃ == 0 is required"
                )
            end
        end

        if ρ_π <= 0 || ρ_π >= 1
            error("expected ρ_π ∈ (0, 1) but got $ρ_π")
        end

        if ρ_π̃ < 0 || ρ_π̃ > 1
            error("expected ρ_π̃ ∈ [0, 1) but got $ρ_π̃")
        elseif ρ_π̃ == 0 && π̃ !== nothing
            error("when ρ_π̃ == 0, π̃ === nothing is required")
        end

        # If we are in the discrete-action setting and π̃ is nothing, then we use a proposal
        # policy which iterates over actions
        if π̃ === nothing && discrete(action_space(env))
            if π̃_update_type != None
                error("cannot use proposal update strategy $π̃_update_type with no " *
                    "proposal policy"
                )
            end

            if ρ_π != 0
                error("expected ρ_π == 0 but got $ρ_π")
            end

        elseif π̃ === nothing && continuous(action_space(env))
            π̃ = deepcopy(π)
            if π̃_opt === nothing
                π̃_opt = deepcopy(π_opt)
            end
        end

        if continuous(action_space(env)) && π̃_update_type == None
            error("can only use None update type for proposal policy with discrete actions")
        end

        # Ensure the critic and action space match
        if discrete(critic) && continuous(action_space(env))
            error("cannot use discrete critic for continuous action space")
        elseif continuous(critic) && discrete(action_space(env))
            error("cannot use continuous critic for discrete action space")
        end

        if discrete(π) && num_samples > 0
            @warn "ignoring num_samples with discrete policy"
        end

        if num_samples < 1
            error("num_samples must be >= 0")
        end

        if batch_size < 0
            error("batch size must be > 0")
        end

        if target_refresh_rate <= 0
            error("expected target_refresh_rate > 0 but got $target_refresh_rate")
        end

        if steps_before_learning === nothing
            steps_before_learning = batch_size
        elseif steps_before_learning < 0
            error("expected steps_before_learning >= 0 but got $steps_before_learning")
        end

        soft_q = τ_π != 0

        return new{F,Π,ΠO,Π̃,Π̃O,V,VO,E,ER,R}(
            env,
            rng,
            π,
            π_opt,
            π̃,
            π̃_opt,
            π̃_update_type::UpdateStrategy,
            critic,
            critic_opt,
            critic_target,
            soft_q,
            replay,
            batch_size,
            τ_π,
            τ_π̃,
            π_entropy_actions,
            π̃_entropy_actions,
            p_critic,
            p_π̃,
            target_refresh_rate,
            0,
            steps_before_learning,
            0,
            num_samples,
            ρ_π,
            ρ_π̃,
            true,
        )
    end
end

function CCEM{F}(
        env,
        rng;
        π,
        π_opt,
        π̃ = nothing,
        π̃_opt = nothing,
        π̃_update_type::UpdateStrategy,
        critic,
        critic_opt,
        replay,
        batch_size::Int,
        τ_π = 0f0,
        τ_π̃,
        π_entropy_actions = 0,
        π̃_entropy_actions = 1,
        p_critic,
        p_π̃,
        target_refresh_rate::Int = 1,
        steps_before_learning = nothing,
        num_samples::Int = 30,
        ρ_π = 0.1f0,
        ρ_π̃ = 0.1f0,
) where {F<:AbstractFloat}
    critic_target = deepcopy(critic)

    return CCEM{F}(
        env,
        rng,
        π,
        π_opt,
        π̃,
        π̃_opt,
        π̃_update_type::UpdateStrategy,
        critic,
        critic_opt,
        critic_target,
        replay,
        batch_size,
        τ_π,
        τ_π̃,
        π_entropy_actions,
        π̃_entropy_actions,
        p_critic,
        p_π̃,
        target_refresh_rate,
        steps_before_learning,
        num_samples,
        ρ_π,
        ρ_π̃,
    )
end

function CCEM(
        env,
        rng;
        π,
        π_opt,
        π̃ = nothing,
        π̃_opt = nothing,
        π̃_update_type::UpdateStrategy,
        critic,
        critic_opt,
        replay,
        batch_size::Int,
        τ_π = 0f0,
        τ_π̃,
        π_entropy_actions = 0,
        π̃_entropy_actions = 1,
        p_critic,
        p_π̃,
        target_refresh_rate::Int = 1,
        steps_before_learning = nothing,
        num_samples::Int = 30,
        ρ_π = 0.1f0,
        ρ_π̃ = 0.1f0,
)
    return CCEM{Float32}(
        env,
        rng;
        π = π,
        π_opt = π_opt,
        π̃ = π̃,
        π̃_opt = π̃_opt,
        π̃_update_type = π̃_update_type,
        critic = critic,
        critic_opt = critic_opt,
        replay = replay,
        batch_size = batch_size,
        τ_π = τ_π,
        τ_π̃ = τ_π̃,
        π_entropy_actions = π_entropy_actions,
        π̃_entropy_actions = π̃_entropy_actions,
        p_critic = p_critic,
        p_π̃ = p_π̃,
        target_refresh_rate = target_refresh_rate,
        steps_before_learning = steps_before_learning,
        num_samples = num_samples,
        ρ_π = ρ_π,
        ρ_π̃ = ρ_π̃,
    )
end

RLCore.istrain(c::CCEM) = c._is_training
RLCore.reset!(::CCEM) = nothing

function RLCore.train!(c::CCEM)
    trainmode!(c._π)
    trainmode!(c._π̃)
    trainmode!(c._critic)
    c._is_training = true
end

function RLCore.eval!(c::CCEM)
    testmode!(c._π)
    testmode!(c._π̃)
    testmode!(c._critic)
    c._is_training = false
end

function RLCore.select_action(c::CCEM, state)
    c._current_step += 1
    if continuous(c._π)
        return dropdims(c._π(c._rng, state); dims=2)
    else
        return c._π(c._rng, state)
    end
end

function RLCore.algostep!(c::CCEM, state, action, reward, next_state, γ, done)::Nothing
    push!(c._replay, state, action, reward, next_state, γ, done)

    if c._current_step < c._steps_before_learning
        return
    end

    states, actions, rewards, next_states, γs, dones = rand(
        c._rng,
        c._replay,
        c._batch_size,
    )

    if c._batch_size == 1
        # Unsqueeze batch dimension
        rewards = [rewards]
        γs = [γs]
        states = unsqueeze(states; dims = ndims(states) + 1)
        actions = unsqueeze(actions; dims = ndims(actions) + 1)
        next_states = unsqueeze(next_states; dims = ndims(next_states) + 1)
    end

    c._current_update += 1

    _update_critic!(c, states, actions, rewards, next_states, γs)
    _update_actor!(c, states)

    return nothing
end

function _update_actor!(c::CCEM, states)::Nothing
    cont = continuous(action_space(c._env))
    if cont || (discrete(action_space(c._env)) && c._π̃ !== nothing)
        _update_π_π̃!(c, states)
    else
        _update_π!(c, states)
    end

    nothing
end

function _update_π_π̃!(c::CCEM, states)::Nothing
    state_size = size(observation_space(c._env))
    action_size = size(action_space(c._env))[1] # Assume actions are vectors

    # Sample actions from π̃ for the CCEM update
    actions = c._π̃(c._rng, states; num_samples=c._num_samples)  # (𝒜, num_samples, batch)
    actions = reshape(actions, action_size..., :)

    # Stack states to calculate action values. Each sampled action requires one state
    # observation
    # stacked_states = reshape(states, size(states)[begin:end-1]..., 1, size(states)[end])
    # stacked_states = repeat(
    #     states,
    #     ones(Int, length(state_size))...,
    #     c._num_samples,
    #     1,
    # )
    # stacked_states = reshape(stacked_states, state_size..., :)
    stacked_states = repeat(
        state;
        inner = (ones(Int, length(state_size))..., c._num_samples),
    )

    q = c._critic(stacked_states, actions)
    q = reshape(q, c._num_samples, c._batch_size)
    ind = mapslices(x -> sortperm(x; rev=true), q; dims=1)

    # Actor π update
    ∇lnπ = let
        # Calculate how many actions of maximal value to increase the log-likelihood of
        π_n = trunc(Int, c._ρ_π * c._num_samples)

        # Get the π_n actions of maximal value
        π_top_ind = [
            CartesianIndex(ind[i, j], j) for
            i in 1:π_n,
            j in 1:c._batch_size
        ]
        π_top_actions = reshape(
            actions,
            action_size, c._num_samples, c._batch_size,
        )[:, π_top_ind]
        π_top_actions = reshape(π_top_actions, action_size, :)

        use_ℍπ = c._π_entropy_actions > 0 && c._τ_π > 0
        if use_ℍπ
            ℍπ_actions = c._π(c._rng, states; num_samples=c._π_entropy_actions)
            ℍπ_actions = reshape(ℍπ_actions, action_size..., :)


            # ℍπ_states = reshape(states, size(states)[begin:end-1]..., 1, size(states)[end])
            # ℍπ_states = repeat(
            #     ℍπ_states,
            #     ones(Int, length(state_size))...,
            #     c._π_entropy_actions,
            #     1,
            # )
            # ℍπ_states = reshape(ℍπ_states, state_size..., :)
            ℍπ_states = repeat(
                states;
                inner = (ones(Int, length(state_size))..., c._π_entropy_actions),
            )
        end

        # π_states = reshape(states, size(states)[begin:end-1]..., 1, size(states)[end])
        # π_states = repeat(
        #     π_states,
        #     ones(Int, length(state_size))...,
        #     π_n,
        #     1,
        # )
        # π_states = reshape(π_states, state_size..., :)
        π_states = repeat(
            states;
            inner = (ones(Int, length(state_size))..., π_n),
        )

        gradient(Flux.params(c._π)) do
            lnπ = logprob(c._π, π_states, π_top_actions)

            if use_ℍπ
                ℍπ = logprob(c._π, ℍπ_states, ℍπ_actions)
                ℍπ = -mean(ℍπ)
            else
                ℍπ = 0
            end

            πloss = mean(lnπ) + c._τ_π * ℍπ
            return -πloss
        end
    end
    Flux.Optimise.update!(c._π_opt, Flux.params(c._π), ∇lnπ)

    # Update the proposal policy
    if c._π̃_update_type == TargetStrategy
        polyak!(c._p_π̃, c._π̃, c._π)
        return nothing
    elseif c._π̃_update_type == None
        return nothing
    end
    # Else, we are using an actor-like update, implemented below

    # Proposal π̃ update
    ∇lnπ̃ = let
        # Calculate how many actions of maximal value to increase the log-likelihood of
        π̃_n = trunc(Int, c._ρ_π̃ * c._num_samples)

        # Get the π̃_n actions of maximal value
        π̃_top_ind = [
            CartesianIndex(ind[i, j], j) for
            i in 1:π̃_n,
            j in 1:c._batch_size
        ]
        π̃_top_actions = reshape(
            actions,
            action_size, c._num_samples, c._batch_size,
        )[:, π̃_top_ind]
        π̃_top_actions = reshape(π̃_top_actions, action_size, :)

        use_ℍπ̃ = c._π̃_entropy_actions > 0 && c._τ_π̃ > 0
        if use_ℍπ̃
            ℍπ̃_actions = c._π̃(c._rng, states; num_samples=c._π̃_entropy_actions)
            ℍπ̃_actions = reshape(ℍπ̃_actions, action_size..., :)

            # ℍπ̃_states = reshape(states, size(states)[begin:end-1]..., 1, size(states)[end])
            # ℍπ̃_states = repeat(
            #     ℍπ̃_states,
            #     ones(Int, length(state_size))...,
            #     c._π_entropy_actions,
            #     1,
            # )
            # ℍπ̃_states = reshape(ℍπ̃_states, state_size..., :)
            ℍπ̃_states = repeat(
                states;
                inner = (ones(Int, length(state_size))..., c._π_entropy_actions),
            )
        end

        # π̃_states = reshape(states, size(states)[begin:end-1]..., 1, size(states)[end])
        # π̃_states = repeat(
        #     π̃_states,
        #     ones(Int, length(state_size))...,
        #     π̃_n,
        #     1,
        # )
        # π̃_states = reshape(π̃_states, state_size..., :)
        π̃_states = repeat(
            states;
            inner = (ones(Int, length(state_size))..., π̃_n),
        )

        gradient(Flux.params(c._π̃)) do
            lnπ̃ = logprob(c._π̃, π̃_states, π̃_top_actions)

            if use_ℍπ̃
                ℍπ̃ = logprob(c._π̃, ℍπ̃_states, ℍπ̃_actions)
                ℍπ̃ = -mean(ℍπ̃)
            else
                ℍπ̃ = 0
            end

            π̃loss = mean(lnπ̃) + c._τ_π̃ * ℍπ̃
            return -π̃loss
        end
    end
    Flux.Optimise.update!(c._π̃_opt, Flux.params(c._π̃), ∇lnπ̃)

    return nothing
end

function _update_π!(c::CCEM, states)::Nothing
    if c._π̃_update_type != None
        error("call _update_π_π̃! when π̃_update_type != None")
    end

    n = action_space(c._env).n[1]
    q = c._critic(states)

    # Find the indices of the actions of maximal value
    ind = mapslices(x -> sortperm(x; rev=true), q; dims=1)
    top_ind = [CartesianIndex(ind[1, j], j) for j in 1:c._batch_size]

    use_entropy = c._π_entropy_actions > 0 && c._τ_π > 0
    if use_entropy
        # Randomly select actions from π to estimate the entropy
        entropy_ind = [
            CartesianIndex(c._π(c._rng, states[:, j])[1], j) for
            _ in 1:c._π_entropy_actions,
            j in 1:c._batch_size
        ]
    end

    gs = gradient(Flux.params(c._π)) do
        log_prob = logprob(c._π, states)

        if use_entropy
            # ∇ℍ(π) = 𝔼_{π}[ln(π) ∇ln(π)] (remember 𝔼_{π}[∇ln(π)] = 0)
            entropy = log_prob[entropy_ind]
            entropy = entropy .* Zygote.dropgrad(entropy)
            entropy = -mean(entropy)
            println(entropy)
        else
            entropy = 0
        end

        # Compute the gradient ∇J = 𝔼_{I*}[ln(π)] + τℍ(π)
        log_prob = mean(log_prob[top_ind])
        loss = log_prob + c._τ_π * entropy
        -loss
    end
    Flux.Optimise.update!(c._π_opt, Flux.params(c._π), gs)

    return nothing
end

function _update_critic!(c::CCEM, states, actions, rewards, next_states, γs)::Nothing
    # Calculate the q-values of actions (drawn from the current policy) in the next states
    if continuous(c._π)
        next_actions = dropdims(c._π(c._rng, next_states); dims=2)
    else
        next_actions = c._π(c._rng, next_states)
    end
    next_q_values = c._critic_target(next_states, next_actions)

    # Adjust the next q values if using soft action-value functions
    if c._soft_q
        logπ = logprob(c._π, next_states, next_actions)

        # StableBaselines3 implementation does this
        next_q_values .-= c._τ_π .* logπ
    end

    # Update the critic
    gs = gradient(Flux.params(c._critic)) do
        loss(c._critic, states, actions, rewards, γs, next_q_values)
    end
    Flux.Optimise.update!(c._critic_opt, Flux.params(c._critic), gs)

    # Update the target critic
    if (c._current_update % c._target_refresh_rate) == 0
        polyak!(c._p_critic, c._critic_target, c._critic)
    end

    return nothing
end

function Base.show(io::IO, ::CCEM)
    print(io, "CCEM")
end
