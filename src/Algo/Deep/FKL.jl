"""
# Description

FKL implements a likelihood based actor-critic algorithm with entropy
regularization and experience replay. The policy improvement step is
accomplished by minimizing FKL(π, ℬQ), where ℬQ is the Boltzmann
distribution over action values [1].

## Updates

This section discusses which update targets are used to update the actor and critic, as well
as some implementation details on how these updates are performed.

Because this implementation solely follows [1], the temperature τ is the entropy scale,
rather than a reward scale as in [2, 3].

### Actor Update

For discrete actions, this algorithm uses the gradient in equation 7
in [1]:
    ∇FKL(π, ℬQ) = - Σₐ ℬQ(a | s) ∇ln(π(a | s))
where:
    ∇ = ∇_ϕ
    π = π_π

When the temperature τ is 0, the update becomes the hard FKL update, found in equation 9 in
[1]:
    ∇HardFKL(π, ℬQ) = - ∇ln(π( argmaxₐ q(s, a) | s))
where:
    ∇ = ∇_ϕ
    π = π_π
    q = action-value function approximation

For continuous actions, we can no longer sum over all actions, instead, we
use a sampled gradient as outlined in equation (14) of [1]:
    ∇FKL(π, ℬQ) = - ∑₁ⁿ ρ̃ ∇ln(π(a∣s))
where:
    ρ_i = ℬQ(a_i | s) / π_θ(a_i | s) ∝ exp(Q(s, a_i)τ⁻¹) / π(a_i | s)
    ρ̃_i = ρ_i / ∑(ρ_j)
    ∇ = ∇_ϕ
    π = π_ϕ

### Critic Update

To learn a critic, this algorithm uses a Sarsa update.

When learning soft Q functions, this class uses the following update target, where the
temperature τ is treated as an entropy scale as in [1] (rather than a reward scale as can
be found in [2, 3]):
    q(s, a) = 𝔼[r + γ(q(s', a') - τ ln(π(a|s))]
and otherwise uses the "regular" Sarsa update target:
    q(s, a) = 𝔼[r + γq(s', a')]

## References
[1] Greedification Operators for Policy Optimization: Investigating Forward
and Reverse KL Divergences. Chan, A., Silva H., Lim, S., Kozuno, T.,
Mahmood, A. R., White, M. 2021.

[2] Soft Actor-Critic: Off-Policy Maximum Entropy Deep Reinforcement
Learning with a Stochastic Actor. Haarnoja, T., Zhou, A., Abbeel, P.,
Levine, S. International Conference on Machine Learning. 2018.

[3] Soft Actor-Critic: Algorithms and Applications. Haarnoja, T.,
Zhou, A., Hartikainen, K., Tucker, G., Ha, S., Tan, J., Kumar, V.,
Zhu, H., Gupta, A., Abbeel, P., Levine, S. In preparation. 2019.
"""
mutable struct FKL{
        F<:AbstractFloat,           # Floating point type
        Π<:AbstractPolicy,          # Policy type
        ΠO,                         # Policy optimizer
        V<:AbstractValueFunction,   # Value function type
        VO,                         # Value function optimizer
        E<:AbstractEnvironment,     # Environment type
        ER<:AbstractReplay,         # Experience replay
} <: AbstractAlgo
    _env::E
    _rng::AbstractRNG

    _π::Π
    _π_opt::ΠO

    _critic::V
    _critic_opt::VO
    _critic_target::V

    _soft_q::Bool

    _replay::ER
    _batch_size::Int

    _τ::F # Temperature
    _p::F # Polyak averaging constant
    _target_refresh_rate::Int
    _current_update::Int

    _scale_actor_lr_by_temperature::Bool
    _steps_before_learning::Int
    _current_step::Int

    _num_samples::Int

    _is_training::Bool

    function FKL{F}(
        env::E,
        rng,
        π::Π,
        π_opt::ΠO,
        critic::V,
        critic_opt::VO,
        critic_target::V,
        soft_q,
        replay::ER,
        batch_size,
        τ,
        p,
        target_refresh_rate,
        scale_actor_lr_by_temperature,
        steps_before_learning,
        num_samples,
    ) where {
        F<:AbstractFloat,
        Π<:AbstractPolicy,
        ΠO,
        V<:AbstractValueFunction,
        VO,
        E<:AbstractEnvironment,
        ER<:AbstractReplay,
    }
        # Ensure the policy and action space match
        if discrete(π) && continuous(action_space(env))
            error("cannot use discrete policy for continuous action space")
        elseif continuous(π) && discrete(action_space(env))
            error("cannot use continuous policy for discrete action space")
        end

        # Ensure the critic and action space match
        if discrete(critic) && continuous(action_space(env))
            error("cannot use discrete critic for continuous action space")
        elseif continuous(critic) && discrete(action_space(env))
            error("cannot use continuous critic for discrete action space")
        end

        if scale_actor_lr_by_temperature && τ == 0
            error("cannot scale actor stepsize by entropy scale of 0")
        end

        if discrete(π) && num_samples > 0
            @warn "ignoring num_samples with discrete policy"
        end
        if num_samples < 1
            error("num_samples must be > 0")
        end

        if batch_size < 0
            error("batch size must be > 0")
        end

        if target_refresh_rate <= 0
            error("expected target_refresh_rate > 0 but got $target_refresh_rate")
        end

        if τ == 0 && continuous(π)
            error("cannot use hard FKL with continuous policies")
        end

        if steps_before_learning === nothing
            steps_before_learning = batch_size
        elseif steps_before_learning < 0
            error("expected steps_before_learning >= 0 but got $steps_before_learning")
        end

        return new{F,Π,ΠO,V,VO,E,ER}(
            env,
            rng,
            π,
            π_opt,
            critic,
            critic_opt,
            critic_target,
            soft_q,
            replay,
            batch_size,
            τ,
            p,
            target_refresh_rate,
            0,
            scale_actor_lr_by_temperature,
            steps_before_learning,
            0,
            num_samples,
            true,
        )
    end
end

function FKL{F}(
        env,
        rng;
        π,
        π_opt,
        critic,
        critic_opt,
        replay,
        batch_size::Int,
        τ,
        p,
        target_refresh_rate::Int = 1,
        steps_before_learning = nothing,
        num_samples::Int = 30,
        soft_q::Bool = true,
        scale_actor_lr_by_temperature::Bool = true,
) where {F<:AbstractFloat}
    critic_target = deepcopy(critic)

    return FKL{F}(
        env,
        rng,
        π,
        π_opt,
        critic,
        critic_opt,
        critic_target,
        soft_q,
        replay,
        batch_size,
        τ,
        p,
        target_refresh_rate,
        scale_actor_lr_by_temperature,
        steps_before_learning,
        num_samples,
    )
end

function FKL(
        env::AbstractEnvironment,
        rng::AbstractRNG;
        π,
        π_opt,
        critic,
        critic_opt,
        replay,
        batch_size::Int,
        τ,
        p,
        target_refresh_rate::Int = 1,
        steps_before_learning = nothing,
        num_samples::Int = 30,
        soft_q::Bool = true,
        scale_actor_lr_by_temperature::Bool = true,
)
    return FKL{Float32}(
        env,
        rng;
        π = π,
        π_opt = π_opt,
        critic = critic,
        critic_opt = critic_opt,
        replay = replay,
        batch_size = batch_size,
        τ = τ,
        p = p,
        target_refresh_rate = target_refresh_rate,
        steps_before_learning = steps_before_learning,
        num_samples = num_samples,
        soft_q = soft_q,
        scale_actor_lr_by_temperature = scale_actor_lr_by_temperature,
    )
end

RLCore.istrain(f::FKL) = f._is_training
RLCore.reset!(::FKL) = nothing

function RLCore.train!(f::FKL)
    trainmode!(f._π)
    trainmode!(f._critic)
    f._is_training = true
end

function RLCore.eval!(f::FKL)
    testmode!(f._π)
    testmode!(f._critic)
    f._is_training = false
end

function RLCore.select_action(f::FKL, state)
    f._current_step += 1
    if continuous(f._π)
        return dropdims(f._π(f._rng, state); dims=2)
    else
        return f._π(f._rng, state)
    end
end

function RLCore.algostep!(f::FKL, state, action, reward, next_state, γ, done)::Nothing
    push!(f._replay, state, action, reward, next_state, γ, done)

    if f._current_step < f._steps_before_learning
        return
    end

    states, actions, rewards, next_states, γs, dones = rand(
        f._rng,
        f._replay,
        f._batch_size,
    )

    if f._batch_size == 1
        # Unsqueeze batch dimension
        rewards = [rewards]
        γs = [γs]
        states = unsqueeze(states; dims = ndims(states) + 1)
        actions = unsqueeze(actions; dims = ndims(actions) + 1)
        next_states = unsqueeze(next_states; dims = ndims(next_states) + 1)
    end

    f._current_update += 1

    _update_critic!(f, states, actions, rewards, next_states, γs)
    _update_actor!(f, states)

    return nothing
end

function _update_actor!(f::FKL, states)::Nothing
    if RLCore.continuous(action_space(f._env))
        _update_actor_continuous!(f, states)
    else
        _update_actor_discrete!(f, states)
    end

    return nothing
end

function _update_actor_continuous!(f::FKL, state_batch)::Nothing
    batch_size = f._batch_size
    state_size = size(observation_space(f._env))
    action_size = size(action_space(f._env))

    # Sample actions for each state in the batch, needed to estimate the gradient in each
    # state
    actions = rand(f._rng, f._π, state_batch; num_samples=f._num_samples)
    actions = reshape(actions, action_size..., :)

    # states = reshape(state_batch, state_size..., 1, batch_size)
    # states = repeat(states, ones(Int, length(state_size))..., f._num_samples, 1)
    # states = reshape(states, state_size..., :)
    states = repeat(
        state_batch;
        inner = (ones(Int, length(state_size))..., f._num_samples),
    )


    q = reshape(f._critic(states, actions), f._num_samples, batch_size)
    prob = exp.(logprob(f._π, states, actions))
    prob = reshape(prob, f._num_samples, batch_size)

    # Calculate the weighted importance sampling ratio
    # Right now, we follow equation (14) in [1] to compute the
    # weighted importance sampling ratio, where:
    #
    # ρ_i = BQ(a_i | s) / π_θ(a_i | s) ∝ exp(Q(s, a_i)τ⁻¹) / π(a_i | s)
    # ρ̂_i = ρ_i / ∑(ρ_j)
    #
    # We could compute a more numerically stable weighted importance
    # sampling ratio if needed (but the implementation is very
    # complicated):
    #
    # ρ̂ = π(a_i | s) [∑_{i≠j} ([h(s, a_j)/h(s, a_i)] * π(a_j | s)⁻¹) + 1]
    # h(s, a_j, a_i) = exp[(Q(s, a_j) - M)τ⁻¹] / exp[(Q(s, a_i) - M)τ⁻¹]
    # M = M(a_j, a_i) = max(Q(s, a_j), Q(s, a_i))
    weighted_lr = let
        likelihood_ratio = q ./ f._τ
        max_lr = maximum(likelihood_ratio; dims=1)
        likelihood_ratio .-= max_lr
        likelihood_ratio = exp.(likelihood_ratio)

        z = sum(likelihood_ratio; dims=1)
        likelihood_ratio ./= z
        likelihood_ratio ./= prob

        weight = sum(likelihood_ratio; dims=1)

        likelihood_ratio ./ weight
    end

    # Compute the gradient of the FKL objective
    gs = gradient(Flux.params(f._π)) do
        logπ = logprob(f._π, states, actions)
        logπ = reshape(logπ, f._num_samples, batch_size)

        loss = weighted_lr .* logπ
        loss = -mean(sum(loss; dims=1))

        if f._scale_actor_lr_by_temperature
            loss *= f._τ
        end

        return loss
    end

    Flux.Optimise.update!(f._π_opt, Flux.params(f._π), gs)

    return nothing
end

function _update_actor_discrete!(f::FKL, state_batch)::Nothing
    # If the policy is discrete, we compute the gradient as equation (7)
    # in [1] by iterating over the actions. If using the hard FKL then
    # equation (9) is used
    if f._τ != 0 # Soft FKL
        q = f._critic(state_batch)
        q ./= f._τ
        ℬq = softmax(q; dims=1)

        gs = gradient(Flux.params(f._π)) do
            logπ = logprob(f._π, state_batch)
            loss = -mean(sum(ℬq .* logπ; dims=1))

            if f._scale_actor_lr_by_temperature
                loss *= f._τ
            end

            return loss
        end

    else # Hard FKL
        q = f._critic(state_batch)
        maximal_actions = mapslices(argmax, q; dims=1)

        gs = gradient(Flux.params(f._π)) do
            logπ = logprob(f._π, state_batch, maximal_actions)
            loss = -mean(logπ)

            return loss
        end
    end

    Flux.Optimise.update!(f._π_opt, Flux.params(f._π), gs)

    return nothing
end

function _update_critic!(f::FKL, states, actions, rewards, next_states, γs)::Nothing
    # Calculate the q-values of actions (drawn from the current policy) in the next states
    if continuous(f._π)
        next_actions = dropdims(f._π(f._rng, next_states); dims=2)
    else
        next_actions = f._π(f._rng, next_states)
    end
    next_q_values = f._critic_target(next_states, next_actions)

    if f._soft_q
        # Adjust the next q values if using soft action-value functions as in [1]
        logπ = logprob(f._π, next_states, next_actions)

        next_q_values .-= f._τ .* logπ
    end

    # Update the critic
    gs = gradient(Flux.params(f._critic)) do
        loss(f._critic, states, actions, rewards, γs, next_q_values)
    end
    Flux.Optimise.update!(f._critic_opt, Flux.params(f._critic), gs)

    # Update the target critic
    if (f._current_update % f._target_refresh_rate) == 0
        polyak!(f._p, f._critic_target, f._critic)
    end

    return nothing
end

function Base.show(io::IO, ::FKL)
    print(io, "FKL")
end
