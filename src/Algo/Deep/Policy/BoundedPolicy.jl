"""
    BoundedPolicy{F,D,A} <: IIDPolicy{F,D,A}

A BoundedPolicy is a policy whose underlying distribution has bounded support.
All action dimensions are parameterized by one of these distributions and are iid.
Any distribution with bounded support can be turned into a policy using a `BoundedPolicy`.

For example, if the underlying distribution is Kumaraswamy, then each action dimension is
drawn iid from a Kumaraswamy distribution.

# Detailed Description

## Convenience Constructors

We implement a number of constructors to conveniently construct `BoundedPolicy`s:

BetaPolicy(args...; kwargs...)
KumaraswamyPolicy(args...; kwargs...)
LogitNormalPolicy(args...; kwargs...)
ArctanhNormalPolicy(args...; kwargs...)

All these constructors have the same arguments:
    - F::Type{<:AbstractFloat} - The type of floating point precision to use, optional
    - env::AbstractEnvironment - The environment to run the policy on
    - approx - The function approximator which outputs the policy parameters

and keyword arguments:
    - eval_method::Symbol = :rand - The method of action selection for offline evaluation

## Offline Evaluation

Many Actor-Critic algorithms are evaluated offline, where every so often an evaluation phase
is run to evaluate the performance of the learned policy. During this evaluation phase,
generally the mean, median, or modal action is used to select actions. The keyword argument
`eval_method` determines whether the policy should use the mean, median, or modal action
during an evaluation phase or should simply sample from the policy, including all stochastic
noise. Not all policies can use all of these methods. For example, the Logit Normal
distribution has no closed form for its mean or mode, so only the median action can be used
during the evaluation phase.

There are four options for the keyword argument `eval_method`:
    - :mean - use the mean action
    - :mode - use the modal action
    - :median - use the median action
    - :rand - sample from the policy, including all stochastic noise

All policies can use the `:rand` method.

## Reparameterized Sampling

To implement reparameterized action sampling for most policies, we use the fact that if `u ~
U(0, 1)`, then `x = F⁻¹(u)` follows the distribution with cumulative distribution function
(CDF) `F`. For example, if `F` is the CDF for the Beta distribution with shape parameters
`a` and `b`, then `x ~ Beta(a, b)`. This implementation is both correct and flexible. Any
distribution can implement reparameterized sampling this way, but some special distributions
have other methods for reparameterized sampling which may be faster, as discussed below.

### Reparameterized Sampling for Special Policies

For policies with a Hyperbolic Arctangent Normal distributions, we implement reparameterized
sampling in the same way done in [1, 2]. We first sample `ε ~ N(0, 1)`, and then use the
fact that `y = μ + ε * σ ~ N(μ, σ)`. After that, samples from the policy can be computed as
`tanh(y)`.

## References
[1] Soft Actor-Critic: Off-Policy Maximum Entropy Deep Reinforcement
Learning with a Stochastic Actor. Haarnoja, T., Zhou, A., Abbeel, P.,
Levine, S. International Conference on Machine Learning. 2018.

[2] Soft Actor-Critic: Algorithms and Applications. Haarnoja, T.,
Zhou, A., Hartikainen, K., Tucker, G., Ha, S., Tan, J., Kumar, V.,
Zhu, H., Gupta, A., Abbeel, P., Levine, S. In preparation. 2019.
"""
mutable struct BoundedPolicy{
        F<:AbstractFloat,
        D<:RLDist.ContinuousUnivariateDistribution,
        A,
} <: IIDPolicy{F,D,A}
    _approximator::A
    _action_bias::AbstractArray{F,1}
    _action_scale::AbstractArray{F,1}
    _action_min::F
    _action_max::F
    _dist_min::F # Infimum for the support of the underlying distribution
    _dist_max::F # Supremum for the support of the underlying distribution
    _is_training::Bool
    _sum_log_scale::F
    _eval_method::Symbol

    function BoundedPolicy{F,D}(
            approximator::A,
            action_min,
            action_max,
            dist_min,
            dist_max,
            eval_method = :rand,
    ) where {F,D,A}
        if !allequal(action_min)
            error("BoundedPolic expects action_min[i] == action_min[j] ∀i,j")
        end
        if !allequal(action_max)
            error("BoundedPolicy expects action_max[i] == action_max[j] ∀i,j")
        end
        if !(eval_method in (:mode, :mean, :median, :rand))
            error("eval_method should be one of :mode, :median, :mean, or :rand")
        end

        action_scale = (action_max - action_min) / (dist_max - dist_min)
        action_bias = begin
            -action_scale * dist_min + action_min
        end
        sum_log_scale = sum(log.(action_scale))

        return new{F,D,A}(
            approximator,
            action_bias,
            action_scale,
            action_min[1],
            action_max[1],
            dist_min,
            dist_max,
            true,
            sum_log_scale,
            eval_method,
        )
    end
end

function BoundedPolicy{F,D}(
        env::AbstractEnvironment,
        approximator,
        eval_method::Symbol,
) where {F<:AbstractFloat,D<:ContinuousUnivariateDistribution}
    as = action_space(env)
    action_min = low(as)
    action_max = high(as)

    # Check to make sure the approximator outputs the correct number of values
    out = approximator(rand(observation_space(env)))
    expected = length(params(D()))
    if !(out isa Tuple) || length(out) != expected
        error("expected approximator to output a $expected-Tuple but got $(typeof(out))")
    end
    if size(out[1])[1] != size(as)[1]
        error("expected approximator to output $(size(as)[1]) values for environment " *
            "$env but got $(size(out[1])[1])")
    end

    dist_min, dist_max = extrema(D())

    return BoundedPolicy{F,D}(
        approximator,
        action_min,
        action_max,
        dist_min,
        dist_max,
        eval_method,
    )
end

function BoundedPolicy{F,D}(
    env::AbstractEnvironment,
    approximator,
    eval_method::String,
) where {F<:AbstractFloat,D<:RLDist.ContinuousUnivariateDistribution}
    return BoundedPolicy{F,D}(env, approximator, Symbol(eval_method))
end

# ########################################################################
# Convenience constructors for a number of BoundedPolicy's
# ########################################################################
function BetaPolicy(
        F::Type{<:AbstractFloat},
        env::AbstractEnvironment,
        approx;
        eval_method = :rand,
)
    BoundedPolicy{F,RLDist.Beta}(env, approx, eval_method)
end

function BetaPolicy(
        env::AbstractEnvironment,
        approx;
        eval_method = :rand,
)
    F = eltype(action_space(env))
    return BetaPolicy(
        F, env, approx;
        eval_method = eval_method,
    )
end

function KumaraswamyPolicy(
        F::Type{<:AbstractFloat},
        env::AbstractEnvironment,
        approx;
        eval_method = :rand,
)
    return BoundedPolicy{F,RLDist.Kumaraswamy}(env, approx, eval_method)
end

function KumaraswamyPolicy(
        env::AbstractEnvironment,
        approx;
        eval_method = :rand,
)
    F = eltype(action_space(env))
    return KumaraswamyPolicy(
        F, env, approx;
        eval_method = eval_method,
    )
end

function LogitNormalPolicy(
        F::Type{<:AbstractFloat},
        env::AbstractEnvironment,
        approx;
        eval_method = :rand,
)
    return BoundedPolicy{F,RLDist.LogitNormal}(env, approx, eval_method)
end

function LogitNormalPolicy(
        env::AbstractEnvironment,
        approx;
        eval_method = :rand,
)
    F = eltype(action_space(env))
    LogitNormalPolicy(F, env, approx; eval_method = eval_method)
end

function ArctanhNormalPolicy(
        F::Type{<:AbstractFloat},
        env::AbstractEnvironment,
        approx;
        eval_method = :rand,
)
    return BoundedPolicy{F,RLDist.ArctanhNormal}(env, approx, eval_method)
end

function ArctanhNormalPolicy(env, approx; eval_method = :rand)
    F = eltype(action_space(env))
    ArctanhNormalPolicy(
        F, env, approx;
        eval_method = eval_method,
    )
end
# ########################################################################

@Flux.functor BoundedPolicy

Flux.trainable(p::BoundedPolicy) = Flux.trainable(p._approximator)
RLCore.continuous(::BoundedPolicy) = true
RLCore.discrete(::BoundedPolicy) = false
Base.eltype(::BoundedPolicy{F,D}) where {F,D} = AbstractArray{F}
approximator(p::BoundedPolicy) = p._approximator
clamp_samples(::BoundedPolicy)::Bool = true # For stability when calculating ∇ln(π)
clamp_rsamples(::BoundedPolicy)::Bool = false

function RLCore.train!(p::BoundedPolicy)
    Flux.trainmode!(p)
    p._is_training = true
end

function RLCore.eval!(p::BoundedPolicy)
    Flux.testmode!(p)
    p._is_training = false
end

function Base.in(item, p::BoundedPolicy{F}) where {F}
    return size(item) == size(p) && all(p._action_min .<= item .<= p._action_max)
end

# ########################################################################
# Special implementations of rsample for specific policies
# ########################################################################
function RLCore.rsample(
        p::BoundedPolicy{F,RLDist.ArctanhNormal},
        rng,
        states::AbstractArray{F};
        num_samples = 1,
) where{F,D}
    if !p._is_training
        error("rsample called in eval mode, use sample instead")
    end
    μ, σ = _process_params(p, _get_params(p, states)...)

    actions = if ndims(μ) > 1
        # Sampling actions from a batch of states
        batch_size = size(μ)[end]

        # If in training mode, sample actions from the distribution in each state
        dist = ArctanhNormal.(μ, σ)
        ε = rand(rng, Normal(zero(F), one(F)), size(μ, 1), num_samples, batch_size)
        μ = reshape(μ, size(μ, 1), 1, batch_size)
        σ = reshape(σ, size(μ, 1), 1, batch_size)
        μ .+ ε .* σ
    else
        # Sampling actions from a single state
        ε = rand(rng, Normal(zero(F), one(F)), size(μ, 1), num_samples)
        μ .+ ε .* σ
    end

    return _transform(p, tanh.(actions); clamp_output = clamp_rsamples(p))
end
# ########################################################################

function RLCore.logprob(
        p::BoundedPolicy{F,D},
        states::AbstractArray{F},
        actions::AbstractArray{F},
)::AbstractArray{F} where{F,D}
    params = get_params(p, states)
    return logprob(p, actions, params...)
end

function RLCore.logprob(
        p::BoundedPolicy{F,D},
        actions::AbstractArray{F,N}, # Either a vector or matrix
        params::AbstractArray{F,N}..., # Need one a for each action/action_dim
) where {F,D,N}
    if size(params[1]) != size(actions)
        error("must specify one set of parameters for each action")
    end

    unscaled = _untransform(p, actions; clamp_output = true)

    if ndims(params[1]) == 2
        # In this case, we have a batch of actions to compute the log-prob of
        dist = distribution(p).(params...)
        lp = sum(logpdf.(dist, unscaled); dims = 1) .- p._sum_log_scale
        return lp[1, :] # Return as a vector rather than a matrix
    elseif ndims(params[1]) > 2
        error("logprob expects actions to be a Vector or Matrix")
    else
        # Compute the log-prob of a single action
        dist = distribution(p).(params...)
        return [sum(logpdf.(dist, unscaled)) .- p._sum_log_scale]
    end
end

function _process_params(p::BoundedPolicy{F}, params::AbstractArray{F}...) where {F}
    return params
end

function _transform!(
    p::BoundedPolicy{F}, unscaled::Union{Vector{F},SubArray,Transpose};
    clamp_output,
) where {F}
    broadcast!(*, unscaled, unscaled, p._action_scale)
    broadcast!(+, unscaled, unscaled, p._action_bias)

    if !clamp_output
        return unscaled
    end

    # Due to numerical issues, clamp the actions to stay within the action bounds. If an
    # action is too close to the boundary, it will get rounded to the boundary point (this
    # happens fairly often since we use Float32's). Then, the logpdf of this action has
    # infinite magnitude.
    clamp!(unscaled, p._action_min, p._action_max)
    return nothing
end

function _transform(
    p::BoundedPolicy{F}, unscaled::Union{Vector{F},SubArray,Transpose};
    clamp_output,
) where {F}
    scaled = unscaled .* p._action_scale .+ p._action_bias

    if !clamp_output
        return scaled
    end

    return clamp.(scaled, p._action_min, p._action_max)
end

function _untransform!(
    p::BoundedPolicy{F}, scaled::Union{Vector{F},SubArray,Transpose};
    clamp_output, check = false,
) where {F}
    broadcast!(-, scaled, scaled, p._action_bias)
    broadcast!(*, scaled, scaled, oneunit(F) ./ p._action_scale)

    if !clamp_output
        if check
            min, max = extrema(distribution(p))
            if (any(scaled .>= max || any(scaled .<= min)))
                min = minimum(scaled)
                max = maximum(scaled)
                error("expected unscaled ∈ (0, 1) but got (min, max) = ($min, $max)")
            end
        end
        return nothing
    end

    clamp!(scaled, p._dist_min + eps(F), p._dist_max - eps(F))
    return nothing
end

function _untransform(
    p::BoundedPolicy{F}, scaled::Union{Vector{F},SubArray,Transpose};
    clamp_output, check = false
) where {F}
    unscaled = (scaled .- p._action_bias) ./ p._action_scale

    if !clamp_output
        if check
            min, max = extrema(distribution(p))
            if (any(scaled .>= max || any(scaled .<= min)))
                min = minimum(scaled)
                max = maximum(scaled)
                error("expected unscaled ∈ (0, 1) but got (min, max) = ($min, $max)")
            end
        end
        return unscaled
    end

    return clamp.(unscaled, p._dist_min + eps(F), p._dist_max - eps(F))
end

"""
    _transform(π, unscaled; [kwargs...])

Transform the samples drawn according to the distribution of π to the appropriate
action dimensions.

For example, for a `Beta` policy, samples (from the underlying Beta distribution) are in
`(0, 1)`. This function will transform these samples to be in `(𝒜.min, 𝒜.max)`.
"""
function _transform(π, unscaled::AbstractArray{F,3}; kwargs...) where {F}
    shape = size(unscaled)
    unscaled = reshape(unscaled, shape[1], :)
    return reshape(_transform(π, unscaled; kwargs...), shape)
end

function _transform(π, unscaled::Matrix{F}; kwargs...)::Matrix{F} where {F}
    return reduce(hcat, [_transform(π, col; kwargs...) for col in eachcol(unscaled)])
end

function _transform!(π, unscaled::AbstractArray{F,3}; kwargs...) where {F}
    shape = size(unscaled)
    unscaled = reshape(unscaled, shape[1], :)
    _transform!(π, unscaled; kwargs...), shape
    return nothing
end

function _transform!(π, unscaled::Matrix{F}; kwargs...) where {F}
    for i in 1:size(unscaled, 2)
        _transform!(π, view(unscaled, :, i); kwargs...)
    end
    return nothing
end

"""
    _untransform(π, unscaled; [kwargs...])

Apply the reverse operation of `_transform`, converting action samples (in `[𝒜.min, 𝒜.max]`)
back to samples drawn from the original distribution of π.

For example, for a `Beta` policy, samples (from the underlying Beta distribution) are in
`(0, 1)`. This function will (un)transform these action samples in `(𝒜.min, 𝒜.max)` back to
samples from a Beta distribution in `(0, 1)`.
"""
function _untransform(π, scaled::AbstractArray{F,3}; kwargs...) where {F}
    shape = size(scaled)
    scaled = reshape(scaled, shape[1], :)
    return reshape(_untransform(π, scaled; kwargs...), shape)
end

function _untransform(π, scaled::Matrix{F}; kwargs...)::Matrix{F} where {F}
    return reduce(hcat, [_untransform(π, col; kwargs...) for col in eachcol(scaled)])
end
