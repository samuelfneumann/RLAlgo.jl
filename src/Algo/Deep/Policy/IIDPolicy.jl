"""
    IIDPolicy{F<:AbstractFloat,D,A} <: AbstractPolicy

IIDPolicy implements a policy where each action dimension is sampled iid.
"""
abstract type IIDPolicy{F<:AbstractFloat,D,A} <: AbstractPolicy end

distribution(::IIDPolicy{F,D,A}) where {F,D,A} = D
approximator(p::IIDPolicy) = error("not implemented")
clamp_samples(::IIDPolicy)::Bool = error("not implemented")
clamp_rsamples(::IIDPolicy)::Bool = error("not implemented")
Flux.trainable(::IIDPolicy) = error("not implemented")
Base.eltype(::IIDPolicy{F}) where {F} = AbstractArray{F}
RLCore.continuous(::IIDPolicy) = error("not implemented")
RLCore.discrete(::IIDPolicy) = error("not implemented")

function RLCore.sample(
        p::IIDPolicy{F,D},
        rng,
        states::AbstractArray{F};
        num_samples = 1,
) where{F,D}
    params = get_params(p, states)

    actions = if ndims(params[1]) > 1
        # Sampling actions from a batch of states
        batch_size = size(params[1])[end]

        if !p._is_training
            # If in evaluation mode, simply use the modal action in each state
            action_dims = size(params[1])[begin]
            mode = getproperty(RLDist, p._eval_method).(D.(params...))
            mode = repeat(mode; inner = (1, num_samples))
            reshape(mode, action_dims, num_samples, batch_size)
        else
            # If in training mode, sample actions from the distribution in each state
            dist = distribution(p).(params...)
            samples = Matrix{F}[]
            for i in 1:batch_size
                sample = transpose.(rand.(rng, dist[:, i], num_samples))
                sample = reduce(vcat, sample)
                push!(samples, sample)
            end

            batch(samples)
        end

    else
        # Sampling actions from a single state
        if !p._is_training
            # If in evaluation mode, simply use the modal action
            mode = getproperty(RLDist, p._eval_method).(D.(params...))
            repeat(mode, 1, num_samples)
        else
            # If in training mode, sample actions from the distribution distribution
            dist = D.(params...)
            out = reduce(hcat, rand.(rng, dist, num_samples))

            # Reshape to be (action_dims, num_samples)
            transpose(out)
        end
    end

    # In RLDist, many distributions return Float64's when sampling, even when parameterized
    # with Float32's. Until that is fixed, we need to cast... Notably, the distributions
    # I've found so far are Beta and Gamma. The Beta is due to the Gamma.
    actions = convert.(F, actions)

    _transform!(p, actions; clamp_output = clamp_samples(p))
    return actions
end

function RLCore.rsample(
        p::IIDPolicy{F,D},
        rng,
        states::AbstractArray{F};
        num_samples = 1,
) where{F,D}
    if !p._is_training
        error("rsample called in eval mode, use sample instead")
    end
    params = get_params(p, states)

    actions = if ndims(params[1]) > 1
        # Sampling actions from a batch of states
        batch_size = size(params[1])[end]

        # If in training mode, sample actions in each state
        dist = distribution(P).(params...)
        cdf = rand(rng, F, size(params[1], 1), num_samples, batch_size)
        samples = [quantile.(dist[:, i], cdf[:, :, i]) for i in 1:batch_size]
        batch(samples)

    else
        # Sampling actions from a single state
        cdf = rand(rng, F, size(params[1], 1), num_samples)
        dist = distribution(p).(params...)
        quantile.(dist, cdf)
    end

    return _transform(p, actions; clamp_output = clamp_rsamples(p))
end

function RLCore.logprob(
        p::IIDPolicy{F,D},
        states::AbstractArray{F},
        actions::AbstractArray{F},
)::AbstractArray{F} where{F,D}
    error("not implemented")
end

function get_params(p::IIDPolicy{F}, states::AbstractArray{F}) where {F}
    return _process_params(p, _get_params(p, states)...)
end

function _get_params(p::IIDPolicy{F}, states::AbstractArray{F}) where{F}
    return approximator(p)(states)
end

function _process_params(p::IIDPolicy{F}, params::AbstractArray{F}...) where {F}
    return error("not implemented")
end

function _transform!(
    p::IIDPolicy{F}, unscaled::Union{Vector{F},SubArray,Transpose};
    clamp_output,
) where {F}
    return error("not implemented")
end

function _transform(
    p::IIDPolicy{F}, unscaled::Union{Vector{F},SubArray,Transpose};
    clamp_output,
) where {F}
    return error("not implemented")
end

function _untransform!(
    p::IIDPolicy{F}, scaled::Union{Vector{F},SubArray,Transpose};
    clamp_output, check = false,
) where {F}
    return error("not implemented")
end

function _untransform(
    p::IIDPolicy{F}, scaled::Union{Vector{F},SubArray,Transpose};
    clamp_output, check = false
) where {F}
    return error("not implemented")
end

# """
#     _transform(π, unscaled; [kwargs...])

# Transform the samples drawn according to the distribution of π to the appropriate
# action dimensions.

# For example, for a `Beta` policy, samples (from the underlying Beta distribution) are in
# `(0, 1)`. This function will transform these samples to be in `(𝒜.min, 𝒜.max)`.
# """
# function _transform(π, unscaled::AbstractArray{F,3}; kwargs...) where {F}
#     shape = size(unscaled)
#     unscaled = reshape(unscaled, shape[1], :)
#     return reshape(_transform(π, unscaled; kwargs...), shape)
# end

# function _transform(π, unscaled::Matrix{F}; kwargs...)::Matrix{F} where {F}
#     return reduce(hcat, [_transform(π, col; kwargs...) for col in eachcol(unscaled)])
# end

# function _transform!(π, unscaled::AbstractArray{F,3}; kwargs...) where {F}
#     shape = size(unscaled)
#     unscaled = reshape(unscaled, shape[1], :)
#     _transform!(π, unscaled; kwargs...), shape
#     return nothing
# end

# function _transform!(π, unscaled::Matrix{F}; kwargs...) where {F}
#     for i in 1:size(unscaled, 2)
#         _transform!(π, view(unscaled, :, i); kwargs...)
#     end
#     return nothing
# end

# """
#     _untransform(π, unscaled; [kwargs...])

# Apply the reverse operation of `_transform`, converting action samples (in `[𝒜.min, 𝒜.max]`)
# back to samples drawn from the original distribution of π.

# For example, for a `Beta` policy, samples (from the underlying Beta distribution) are in
# `(0, 1)`. This function will (un)transform these action samples in `(𝒜.min, 𝒜.max)` back to
# samples from a Beta distribution in `(0, 1)`.
# """
# function _untransform(π, scaled::AbstractArray{F,3}; kwargs...) where {F}
#     shape = size(scaled)
#     scaled = reshape(scaled, shape[1], :)
#     return reshape(_untransform(π, scaled; kwargs...), shape)
# end

# function _untransform(π, scaled::Matrix{F}; kwargs...)::Matrix{F} where {F}
#     return reduce(hcat, [_untransform(π, col; kwargs...) for col in eachcol(scaled)])
# end
