# SoftmaxPolicy distribution over N values using floating point precision F and integers
# I
mutable struct SoftmaxPolicy{F<:AbstractFloat,N,I,A} <: AbstractPolicy
    _approximator::A
    _is_training::Bool

    function SoftmaxPolicy{F,N,I}(approximator::A) where {F<:AbstractFloat,N,I<:Integer,A}
        return new{F,N,I,A}(approximator, true)
    end
end


# SoftmaxPolicy is always over discrete variables, so we never include the action shape when
# sampling etc., since it is always 1. This is not the case for continuous action spaces.
function SoftmaxPolicy{F}(env::AbstractEnvironment, approximator) where {F<:AbstractFloat}
    as = action_space(env)

    if !(as isa Spaces.Discrete)
        error("expected action space to be of type RLEnv.Spaces.Discrete but " *
            "got $(typeof(as))")
    elseif ndims(as) > 1
        error("expected a single-dimensional Discrete action space but got $(ndims(as))")
    end

    N = as.n[1]

    # Check to make sure the the approximator outputs the correct number of values
    out = approximator(rand(observation_space(env)))
    if size(out)[1] != N
        error("expected approximator to output $N values for environment $env " *
            "but got $(size(out)[1])")
    end

    SoftmaxPolicy{F,N,Int}(approximator)
end

function SoftmaxPolicy(env::AbstractEnvironment, approximator)
    F = eltype(action_space(env))
    SoftmaxPolicy{F}(env, approximator)
end

@Flux.functor SoftmaxPolicy

Flux.trainable(s::SoftmaxPolicy) = Flux.trainable(s._approximator)
RLCore.continuous(::SoftmaxPolicy) = false
RLCore.discrete(::SoftmaxPolicy) = true
Base.eltype(::SoftmaxPolicy{T,N}) where {T,N} = Int
Base.in(item, ::SoftmaxPolicy{T,N}) where {T,N} = item isa Int && 0 < item <= N

function RLCore.train!(s::SoftmaxPolicy)
    Flux.trainmode!(s)
    s._is_training = true
end

function RLCore.eval!(s::SoftmaxPolicy)
    Flux.testmode!(s)
    s._is_training = false
end

function _get_logits(s::SoftmaxPolicy{T,N}, states) where {T<:AbstractFloat,N}
    logits = s._approximator(states)
end

function RLCore.sample(
        s::SoftmaxPolicy{T,N,I,A},
        rng,
        states::AbstractArray,
        num_samples=1,
) where {T,N,I,A}
    logits = _get_logits(s, states)

    if s._is_training
        if ndims(logits) > 1
            batch_size = size(logits)[end]
            s = [
                    rand(rng, Categorical(softmax(logits[:, i])), num_samples)
                    for i in 1:batch_size
            ]
            return reduce(hcat, s)
        else
            return rand(rng, Categorical(softmax(logits)), num_samples)
        end

    else
        if ndims(logits) > 1
            # Repeat the action of highest probability num_samples times
            batch_size = size(logits)[end]

            s = maximum(logits; dims=1)
            s = reshape(s, 1, 1, batch_size)
            return repeat(s, 1, num_samples, 1)
        else
            return [maximum(logits)]
        end
    end

end

function RLCore.logprob(
        s::SoftmaxPolicy{T},
        states::AbstractArray,
)::AbstractArray{T} where{T}
    logits = _get_logits(s, states)
    return logsoftmax(logits)
end

function RLCore.logprob(
        s::SoftmaxPolicy{T},
        states::AbstractArray,
        actions::AbstractArray{Int},
)::AbstractArray{T} where{T}
    all_probs = logprob(s, states)

    if ndims(all_probs) == 1
        batch_size = size(all_probs)[end]

        return all_probs[[CartesianIndex(actions[i], i) for i in 1:batch_size]]
    elseif ndims(a) > 2
        error("logprob expects actions to be a Vector or Matrix")
    else
        return [all_probs[actions[1]]]
    end
end

function RLCore.logprob(
        s::SoftmaxPolicy{T},
        state::AbstractArray,
        action::Int,
)::T where{T}
    all_probs = logprob(s, state)

    if ndims(all_probs) > 1
        batch_size = size(all_probs)[end]
        error("expected a single state when given a single action but got " *
            "$batch_size states")
    else
        return all_probs[action]
    end
end

