"""
    TruncatedPolicy{F<:AbstractFloat,D,A} <: IIDPolicy{F,D,A}

A TruncatedPolicy is a policy which takes any distribution and truncates it so samples from
the policy stay within the action bounds of the environment. If the truncated distribution
does not have support over the entire action space, then the `TruncatedPolicy` attempts to
scale and shift the truncated distribution to cover the entire action space.


## How is the scale and shift implemented?
"""
mutable struct TruncatedPolicy{F<:AbstractFloat,D,A} <: IIDPolicy{F,D,A}
    _approximator::A
    _action_bias::AbstractArray{F,1}
    _action_scale::AbstractArray{F,1}
    _action_min::F
    _action_max::F
    _dist_min::F
    _dist_max::F
    _is_training::Bool
    _sum_log_scale::F
    _eval_method::Symbol

    function TruncatedPolicy{F,D}(
            approximator::A,
            action_min,
            action_max,
            dist_min,
            dist_max,
            eval_method = :rand,
    ) where {F,D,A}
        if !allequal(action_min)
            error("TruncatedPolicy expects action_min[i] == action_min[j] ∀i,j")
        end
        if !allequal(action_max)
            error("TruncatedPolicy expects action_max[i] == action_max[j] ∀i,j")
        end
        if !(eval_method in (:mode, :mean, :median, :rand))
            error("eval_method should be one of :mode, :median, :mean, or :rand")
        end

        action_scale = (action_max - action_min) / (dist_max - dist_min)
        action_bias = begin
            -action_scale * dist_min + action_min
        end
        sum_log_scale = sum(log.(action_scale))

        if action_min[1] == dist_min[1] && action_max[1] == dist_max[1]
            if any(action_scale .!= 1)
                error("expected all action_scale == 1, got $action_scale")
            end
            if any(action_bias .!= 0)
                error("expected all action_bias == 0, got $action_bias")
            end
            if sum_log_scale != 0
                error("expected sum_log_scale == 0, got $sum_log_scale")
            end
        end

        return new{F,D,A}(
            approximator,
            action_bias,
            action_scale,
            action_min[1],
            action_max[1],
            dist_min,
            dist_max,
            true,
            sum_log_scale,
            eval_method,
        )
    end
end

function TruncatedPolicy{F,D}(
        env::AbstractEnvironment,
        approximator,
        eval_method::Symbol,
) where {F<:AbstractFloat,D}
    as = action_space(env)
    action_min = low(as)
    action_max = high(as)

    # Check to make sure the approximator outputs the correct number of values
    out = approximator(rand(observation_space(env)))
    expected = length(params(D()))
    if !(out isa Tuple) || length(out) != expected
        error("expected approximator to output a $expected-Tuple but got $(typeof(out))")
    end
    if size(out[1])[1] != size(as)[1]
        error("expected approximator to output $(size(as)[1]) values for environment " *
            "$env but got $(size(out[1])[1])")
    end

    if extrema(D()) != (-Inf, Inf)
        min, max = extrema(D())
        @warn "TruncatedPolicy only supports distributions with support on " *
            "ℝ, but you've called it on a distribution with support ($min, $max). " *
            "Trying to shift and scale the distribution to be within the action bounds."

        if (min, max) == (0, 1)
            @warn "Distribution has support in (0, 1), did you mean to use a BoundedPolicy?"
            dist_min, dist_max = extrema(truncated(D(), zero(F), one(F)))

        elseif max == Inf
            @warn "Distribution has support in ($min, ∞), shifting and scaling doesn't " *
                "usually work for these distributions"

            action_range = (action_max .- action_min)[1]
            dist_min, dist_max = extrema(truncated(D(), min, min + action_range))

        elseif min == -Inf
            @warn "Distribution has support in (-∞, $max), shifting and scaling doesn't " *
                "usually work for these distributions"

            action_range = (action_max .- action_min)[1]
            dist_min, dist_max = extrema(truncated(D(), max - action_range, max))

        else
            dist_min, dist_max = extrema(truncated(D(), action_min[1], action_max[1]))
        end
    else
        dist_min, dist_max = extrema(truncated(D(), action_min[1], action_max[1]))
    end

    return TruncatedPolicy{F,D}(
        approximator,
        action_min,
        action_max,
        dist_min,
        dist_max,
        eval_method,
    )
end

function TruncatedPolicy{F,D}(
    env::AbstractEnvironment,
    approximator,
    eval_method::String,
) where {F<:AbstractFloat,D}
    return TruncatedPolicy{F,D}(env, approximator, Symbol(eval_method))
end

# ########################################################################
# Convenience constructors for a number of TruncatedPolicy's
# ########################################################################
function TruncatedNormalPolicy(
        F::Type{<:AbstractFloat},
        env::AbstractEnvironment,
        approx;
        eval_method = :rand,
)
    return TruncatedPolicy{F,Normal}(env, approx, eval_method)
end

function TruncatedNormalPolicy(env, approx; eval_method = :rand)
    F = eltype(action_space(env))
    TruncatedNormalPolicy(F, env, approx; eval_method = eval_method)
end

function TruncatedLaplacePolicy(
        F::Type{<:AbstractFloat},
        env::AbstractEnvironment,
        approx;
        eval_method = :rand,
)
    return TruncatedPolicy{F,Laplace}(env, approx, eval_method)
end

function TruncatedLaplacePolicy(env, approx; eval_method = :rand)
    F = eltype(action_space(env))
    TruncatedLaplacePolicy(F, env, approx; eval_method = eval_method)
end
# ########################################################################

@Flux.functor TruncatedPolicy

Flux.trainable(p::TruncatedPolicy) = Flux.trainable(p._approximator)
RLCore.continuous(::TruncatedPolicy) = true
RLCore.discrete(::TruncatedPolicy) = false
Base.eltype(::TruncatedPolicy{F,D}) where {F,D} = AbstractArray{F}
approximator(p::TruncatedPolicy) = p._approximator
clamp_samples(p::TruncatedPolicy)::Bool = true  # For stability when calculating ∇ln(π)
clamp_rsamples(::TruncatedPolicy)::Bool = false

function distribution(p::TruncatedPolicy{F,D}) where {F,D}
    return (params...) -> truncated(D(params...), p._dist_min, p._dist_max)
end

function RLCore.train!(p::TruncatedPolicy)
    Flux.trainmode!(p)
    p._is_training = true
end

function RLCore.eval!(p::TruncatedPolicy)
    Flux.testmode!(p)
    p._is_training = false
end

function Base.in(item, p::TruncatedPolicy{F}) where {F}
    return size(item) == size(p) && all(p._action_min .<= item .<= p._action_max)
end

function RLCore.logprob(
        p::TruncatedPolicy{F,D},
        states::AbstractArray{F},
        actions::AbstractArray{F},
)::AbstractArray{F} where{F,D}
    params = get_params(p, states)
    return logprob(p, actions, params...)
end

function RLCore.logprob(
        p::TruncatedPolicy{F,D},
        actions::AbstractArray{F,N}, # Either a vector or matrix
        params::AbstractArray{F,N}..., # Need one a for each action/action_dim
) where {F,D,N}
    if size(params[1]) != size(actions)
        error("must specify one set of parameters for each action")
    end

    unscaled = _untransform(p, actions; clamp_output = true)

    if ndims(params[1]) == 2
        # In this case, we have a batch of actions to compute the log-prob of
        dist = distribution(p).(params...)

        if p._sum_log_scale != 0
            lp = sum(logpdf.(dist, unscaled); dims = 1) .- p._sum_log_scale
        else
            lp = sum(logpdf.(dist, unscaled); dims = 1)
        end

        return lp[1, :] # Return as a vector rather than a matrix
    elseif ndims(params[1]) > 2
        error("logprob expects actions to be a Vector or Matrix")
    else
        # Compute the log-prob of a single action
        dist = distribution(p).(params...)
        if p._sum_log_scale != 0
            return [sum(logpdf.(dist, unscaled)) .- p._sum_log_scale]
        else
            return [sum(logpdf.(dist, unscaled))]
        end
    end
end

function _process_params(p::TruncatedPolicy{F}, params::AbstractArray{F}...) where {F}
    return params
end

function _should_transform(p::TruncatedPolicy)::Bool
    return !((p._dist_min, p._dist_max) == (p._action_min, p._action_max))
end

function _transform!(
    p::TruncatedPolicy{F}, unscaled::Union{Vector{F},SubArray,Transpose};
    clamp_output,
) where {F}
    if _should_transform(p)
        broadcast!(*, unscaled, unscaled, p._action_scale)
        broadcast!(+, unscaled, unscaled, p._action_bias)
    end

    if !clamp_output
        return unscaled
    end

    # Due to numerical issues, clamp the actions to stay within the action bounds. If an
    # action is too close to the boundary, it will get rounded to the boundary point (this
    # happens fairly often since we use Float32's). Then, the logpdf of this action has
    # infinite magnitude.
    clamp!(unscaled, p._action_min, p._action_max)
    return nothing
end

function _transform(
    p::TruncatedPolicy{F}, unscaled::Union{Vector{F},SubArray,Transpose};
    kwargs...
) where {F}
    if _should_transform(p)
        scaled = unscaled .* p._action_scale .+ p._action_bias
    else
        scaled = unscaled
    end

    if !clamp_output
        return scaled
    end

    return clamp.(scaled, p._action_min, p._action_max)
end

function _untransform!(
    p::TruncatedPolicy{F}, scaled::Union{Vector{F},SubArray,Transpose};
    clamp_output, check = false,
) where {F}
    if _should_transform(p)
        broadcast!(-, scaled, scaled, p._action_bias)
        broadcast!(*, scaled, scaled, oneunit(F) ./ p._action_scale)
    end

    if !clamp_output
        if check
            min, max = extrema(distribution(p))
            if (any(scaled .>= max || any(scaled .<= min)))
                min = minimum(scaled)
                max = maximum(scaled)
                error("expected unscaled ∈ (0, 1) but got (min, max) = ($min, $max)")
            end
        end
        return nothing
    end

    clamp!(scaled, p._dist_min + eps(F), p._dist_max - eps(F))
    return nothing
end

function _untransform(
    p::TruncatedPolicy{F}, scaled::Union{Vector{F},SubArray,Transpose};
    clamp_output, check = false,
) where {F}
    if _should_transform(p)
        unscaled = (scaled .- p._action_bias) ./ p._action_scale
    else
        unscaled = scaled
    end

    if !clamp_output
        if check
            min, max = extrema(distribution(p))
            if (any(scaled .>= max || any(scaled .<= min)))
                min = minimum(scaled)
                max = maximum(scaled)
                error("expected unscaled ∈ (0, 1) but got (min, max) = ($min, $max)")
            end
        end
        return unscaled
    end

    return clamp.(unscaled, p._dist_min + eps(F), p._dist_max - eps(F))
end
