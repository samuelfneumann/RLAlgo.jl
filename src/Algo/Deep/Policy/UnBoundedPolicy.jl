"""
    UnBoundedPolicy{F<:AbstractFloat,D,A} <: IIDPolicy{F,D,A}

An UnBoundedPolicy is a policy whose underlying distribution is unbounded at one of its
ends. That is, the distribution should be unbounded below or above or both. Actions are
drawn iid from this distribution.

For example, if the underlying distribution is Gamma, then each action dimension is
drawn iid from a Gamma distribution.

# Detailed Description

## Dealing with Distribution Bounds

If the underlying distribution is bounded below or above (but not both),
then the support of the underlying distribution is shifted to cover all of the action space.
For example, if using a Gamma distribution, the distribution is shifted to have support
`(min(𝒜), ∞)`, where `𝒜` is the action space.

If the underlying distribution is unbounded then no shifting will occur.

If the underlying distribution is bounded below **and** above, then an error is raised upon
construction.

## Convenience Constructors

We implement a number of constructors to conveniently construct `UnBoundedPolicy`s:

NormalPolicy(args...; kwargs...)
GammaPolicy(args...; kwargs...)
LogisticPolicy(args...; kwargs...)
LaplacePolicy(args...; kwargs...)

All these constructors have the same arguments:
    - F::Type{<:AbstractFloat} - The type of floating point precision to use, optional
    - env::AbstractEnvironment - The environment to run the policy on
    - approx - The function approximator which outputs the policy parameters

and keyword arguments:
    - eval_method::Symbol = :rand - The method of action selection for offline evaluation
    - clip_action::Bool = true - Whether or not to clip action samples when sampling

## Offline Evaluation

See BoundedPolicy

## Reparameterized Sampling

See BoundedPolicy

### Reparameterized Sampling for Special Policies

For policies with a Normal distributions, we implement reparameterized
sampling in the same way done in [1, 2]. We first sample `ε ~ N(0, 1)`, and then use the
fact that `y = μ + ε * σ ~ N(μ, σ)`.

## References
[1] Soft Actor-Critic: Off-Policy Maximum Entropy Deep Reinforcement
Learning with a Stochastic Actor. Haarnoja, T., Zhou, A., Abbeel, P.,
Levine, S. International Conference on Machine Learning. 2018.

[2] Soft Actor-Critic: Algorithms and Applications. Haarnoja, T.,
Zhou, A., Hartikainen, K., Tucker, G., Ha, S., Tan, J., Kumar, V.,
Zhu, H., Gupta, A., Abbeel, P., Levine, S. In preparation. 2019.
"""
mutable struct UnBoundedPolicy{
        F<:AbstractFloat,
        D,
        A,
} <: IIDPolicy{F,D,A}
    _approximator::A
    _action_min::F
    _action_max::F
    _dist_min::F
    _dist_max::F
    _is_training::Bool
    _eval_method::Symbol
    _clip_action::Bool

    function UnBoundedPolicy{F,D}(
            approximator::A,
            action_min,
            action_max,
            dist_min,
            dist_max,
            eval_method,
            clip_action,
    ) where {F,D,A}
        if !allequal(action_min)
            error("UnBoundedPolicy expects action_min[i] == action_min[j] ∀i,j")
        end
        if !allequal(action_max)
            error("UnBoundedPolicy expects action_max[i] == action_max[j] ∀i,j")
        end
        if !(eval_method in (:mode, :mean, :median, :rand))
            error("eval_method should be one of :mode, :median, :mean, or :rand")
        end

        return new{F,D,A}(
            approximator,
            action_min[1],
            action_max[1],
            dist_min,
            dist_max,
            true,
            eval_method,
            clip_action,
        )
    end
end

function UnBoundedPolicy{F,D}(
        env::AbstractEnvironment,
        approximator,
        eval_method::Symbol,
        clip_action::Bool,
) where {F<:AbstractFloat,D}
    as = action_space(env)
    action_min = low(as)
    action_max = high(as)

    # Check to make sure the approximator outputs the correct number of values
    out = approximator(rand(observation_space(env)))
    expected = length(params(D()))
    if !(out isa Tuple) || length(out) != expected
        error("expected approximator to output a $expected-Tuple but got $(typeof(out))")
    end
    if size(out[1])[1] != size(as)[1]
        error("expected approximator to output $(size(as)[1]) values for environment " *
            "$env but got $(size(out[1])[1])")
    end

    dist_min, dist_max = extrema(D())
    if dist_min > -Inf && dist_max < Inf
        error("distribution is bounded below and above. Perhaps you meant to used " *
            "a BoundedUnBoundedPolicy?")
    end

    return UnBoundedPolicy{F,D}(
        approximator,
        action_min,
        action_max,
        dist_min,
        dist_max,
        eval_method,
        clip_action,
    )
end

function UnBoundedPolicy{F,D}(
    env::AbstractEnvironment,
    approximator,
    eval_method::String,
    clip_action::Bool,
) where {F<:AbstractFloat,D}
    return UnBoundedPolicy{F,D}(env, approximator, Symbol(eval_method), clip_action)
end

# ########################################################################
# Convenience constructors for a number of Policy's
# ########################################################################
function NormalPolicy(
        F::Type{<:AbstractFloat},
        env::AbstractEnvironment,
        approx;
        eval_method = :rand,
        clip_action = true,
)
    return UnBoundedPolicy{F,Normal}(env, approx, eval_method, clip_action)
end

function NormalPolicy(env, approx; eval_method = :rand, clip_action = true)
    F = eltype(action_space(env))
    NormalPolicy(F, env, approx; eval_method = eval_method, clip_action = clip_action)
end

function LaplacePolicy(
        F::Type{<:AbstractFloat},
        env::AbstractEnvironment,
        approx;
        eval_method = :rand,
        clip_action = true,
)
    return UnBoundedPolicy{F,Laplace}(env, approx, eval_method, clip_action)
end

function LaplacePolicy(env, approx; eval_method = :rand, clip_action = true)
    F = eltype(action_space(env))
    LaplacePolicy(F, env, approx; eval_method = eval_method, clip_action = clip_action)
end

function GammaPolicy(
        F::Type{<:AbstractFloat},
        env::AbstractEnvironment,
        approx;
        eval_method = :rand,
        clip_action = true,
)
    return UnBoundedPolicy{F,Gamma}(env, approx, eval_method, clip_action)
end

function GammaPolicy(env, approx; eval_method = :rand, clip_action = true)
    F = eltype(action_space(env))
    GammaPolicy(F, env, approx; eval_method = eval_method, clip_action = clip_action)
end

function LogisticPolicy(
        F::Type{<:AbstractFloat},
        env::AbstractEnvironment,
        approx;
        eval_method = :rand,
        clip_action = true,
)
    return Policy{F,Logistic}(env, approx, eval_method, clip_action)
end

function LogisticPolicy(env, approx; eval_method = :rand, clip_action = true)
    F = eltype(action_space(env))
    LogisticPolicy(F, env, approx; eval_method = eval_method, clip_action = clip_action)
end
# ########################################################################

@Flux.functor UnBoundedPolicy

Flux.trainable(p::UnBoundedPolicy) = Flux.trainable(p._approximator)
RLCore.continuous(::UnBoundedPolicy) = true
RLCore.discrete(::UnBoundedPolicy) = false
Base.eltype(::UnBoundedPolicy{F,D}) where {F,D} = AbstractArray{F}
approximator(p::UnBoundedPolicy) = p._approximator
clamp_samples(p::UnBoundedPolicy)::Bool = p._clip_action
clamp_rsamples(::UnBoundedPolicy)::Bool = clamp_samples(p)

function RLCore.train!(p::UnBoundedPolicy)
    Flux.trainmode!(p)
    p._is_training = true
end

function RLCore.eval!(p::UnBoundedPolicy)
    Flux.testmode!(p)
    p._is_training = false
end

function Base.in(item, p::UnBoundedPolicy{F,D}) where {F,D}
    dist = D()
    min, max = extrema(dist)
    return size(item) == size(p) && all(min .<= item .<= max)
end

function RLCore.rsample(
        p::UnBoundedPolicy{F,RLDist.Normal},
        rng,
        states::AbstractArray{F};
        num_samples = 1,
) where{F,D}
    if !p._is_training
        error("rsample called in eval mode, use sample instead")
    end
    μ, σ = _process_params(p, _get_params(p, states)...)

    actions = if ndims(μ) > 1
        # Sampling actions from a batch of states
        batch_size = size(μ)[end]

        # If in training mode, sample actions from the distribution in each state
        dist = ArctanhNormal.(μ, σ)
        ε = rand(rng, Normal(zero(F), one(F)), size(μ, 1), num_samples, batch_size)
        μ = reshape(μ, size(μ, 1), 1, batch_size)
        σ = reshape(σ, size(μ, 1), 1, batch_size)
        μ .+ ε .* σ
    else
        # Sampling actions from a single state
        ε = rand(rng, Normal(zero(F), one(F)), size(μ, 1), num_samples)
        μ .+ ε .* σ
    end

    return _transform(p, actions; clamp_output = clamp_rsamples(p))
end

function RLCore.logprob(
        p::UnBoundedPolicy{F,D},
        states::AbstractArray{F},
        actions::AbstractArray{F},
)::AbstractArray{F} where{F,D}
params = get_params(p, states)
    return logprob(p, actions, params...)
end

function RLCore.logprob(
        p::UnBoundedPolicy{F,D},
        actions::AbstractArray{F,N}, # Either a vector or matrix
        params::AbstractArray{F,N}..., # Need one a for each action/action_dim
) where {F,D,N}
    if size(params[1]) != size(actions)
        error("must specify one set of parameters for each action")
    end

    unscaled = _untransform(p, actions; clamp_output = true)

    if ndims(params[1]) == 2
        # In this case, we have a batch of actions to compute the log-prob of
        dist = distribution(p).(params...)
        lp = sum(logpdf.(dist, unscaled); dims = 1)
        return lp[1, :] # Return as a vector rather than a matrix
    elseif ndims(params[1]) > 2
        error("logprob expects actions to be a Vector or Matrix")
    else
        # Compute the log-prob of a single action
        dist = distribution(p).(params...)
        return [sum(logpdf.(dist, unscaled))]
    end
end

function _process_params(p::UnBoundedPolicy{F}, params::AbstractArray{F}...) where {F}
    return params
end

_should_shift_down(p::UnBoundedPolicy) = p._action_min < p._dist_min
_should_shift_up(p::UnBoundedPolicy) = p._action_max > p._dist_max

function _should_transform(p::UnBoundedPolicy{F}) where {F}
    if p._dist_min > -Inf && p._dist_max < Inf
        error("should not use bounded distributions with UnBoundedPolicy. Did you " *
            "mean to use a BoundedPolicy?")
    elseif _should_shift_down(p)
        return p._action_min - p._dist_min
    elseif _should_shift_up(p)
        return -p._dist_max + p._action_max
    else
        return zero(F)
    end
end

function _transform!(
    p::UnBoundedPolicy{F}, unshifted::Union{Vector{F},SubArray,Transpose};
    clamp_output,
) where {F}
    shift = _should_transform(p)
    if shift != 0
        unshifted .+= shift
    end

    if !clamp_output
        return unshifted
    end

    clamp!(unshifted, p._action_min, p._action_max)
    return nothing
end

function _transform(
    p::UnBoundedPolicy{F}, unshifted::Union{Vector{F},SubArray,Transpose};
    clamp_output,
) where {F}
    shift = _should_transform(p)
    if shift != 0
        shifted = unshifted .+ shift
    else
        shifted = unshifted
    end

    if !clamp_output
        return unshifted
    end

    return clamp.(unshifted, p._action_min, p._action_max)
end

function _untransform!(
    p::UnBoundedPolicy{F,D}, shifted::Union{Vector{F},SubArray,Transpose};
    clamp_output, check = false,
) where {F,D}
    shift = _should_transform(p)
    if shift != 0
        shifted .-= shift
        if clamp_output
            clamp!(shifted, extrema(D())...)
        end
    end

    # Error checking, ensure all samples are within the distributional support
    if check && !clamp_output
        if _should_shift_down(p)
            min, _ = extrema(distribution(p))
            if any(shifted .<= min)
                got_min = minimum(shifted)
                got_max = maximum(shifted)
                got = (got_min, got_max)
                error("expected shifted ∈ ($min, $max) but got (min, max) = $got")
            end
        elseif _should_shift_up(p)
            _, max = extrema(distribution(p))
            if any(shifted .>= max)
                got_min = minimum(shifted)
                got_max = maximum(shifted)
                got = (got_min, got_max)
                error("expected shifted ∈ ($min, $max) but got (min, max) = $got")
            end
        end
    end

    return shifted
end

function _untransform(
    p::UnBoundedPolicy{F,D}, shifted::Union{Vector{F},SubArray,Transpose};
    clamp_output, check = true,
) where {F,D}
    shift = _should_transform(p)
    if shift != 0
        unshifted = shifted .- shift
        if clamp_output
            unshifted = clamp.(unshifted, extrema(D())...)
        end
    else
        unshifted = shifted
    end

    # Error checking, ensure all samples are within the distributional support
    if check && !clamp_output
        if _should_shift_down(p)
            min, _ = extrema(distribution(p))
            if any(shifted .<= min)
                got_min = minimum(shifted)
                got_max = maximum(shifted)
                got = (got_min, got_max)
                error("expected shifted ∈ ($min, $max) but got (min, max) = $got")
            end
        elseif _should_shift_up(p)
            _, max = extrema(distribution(p))
            if any(shifted .>= max)
                got_min = minimum(shifted)
                got_max = maximum(shifted)
                got = (got_min, got_max)
                error("expected shifted ∈ ($min, $max) but got (min, max) = $got")
            end
        end
    end

    return unshifted
end
