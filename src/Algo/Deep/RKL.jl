"""
# Description
RKL implements an actor-critic algorithm with entropy regularization and experience replay
equivalent to the RKL algorithm in [1] and SAC in [2, 3]. The policy improvement step is
accomplished by minimizing RKL(π, ℬQ), where ℬQ is the Boltzmann
distribution over action values [1]. In the implementation of this RKL algorithm,
the temperature τ can weight the entropy as in [1] or the reward as in [2, 3]:
reward = reward * τ⁻¹. Both are equivalent formulations. This algorithm is equivalent to
SAC [2, 3] when the constructor arguments are chosen appropriately as the RKL algorithm in
[1] is a generalization of SAC [2, 3].

## Updates

This section discusses which update targets are used to update the actor and critic, as well
as some implementation details on how these updates are performed.

This section discusses updates

### Actor Update

For discrete actions, this algorithm uses the gradient in equation 6 in [1]:
    ∇RKL(π, ℬQ) = -Σₐ ∇π(a | s) [ τ⁻¹ Q(s, a) - ln(π(a | s))]           (1)
where:
    ∇ = ∇_ϕ
    π = π_ϕ
    τ = temperature argument

For continuous actions, we can no longer sum over all actions, instead, we
use a sampled gradient as outlined in equation (10) of [1]:
    ∇RKL(π, ℬQ) =                                                       (3)
        -1/n  ∑₁ⁿ [ (Q(s, a) - V(s))(τ⁻¹) - ln(π(a∣s)) ] ∇ln(π(a∣s))
where:
    ∇ = ∇_ϕ
    π = π_ϕ
    τ = temperature argument

In the continuous action setting, a state value baseline (V in equations (3) and (4)
above) can be used. The baseline is approximated as V(s) = 𝔼[q(s, A)] ≈ 1/n ∑ⁿᵢ₌₁ q(s, aᵢ).

In the discrete action setting, no baseline is required because we can compute the
expectation in equations (1) and (2) (equation (6) in [1]) exactly (assuming the true
value function is known).

### Critic Update

To learn a critic, this algorithm uses a Sarsa update.

When learning soft Q functions the algorithm uses the following update targets, where the
temperature τ either scales the entropy as in [1, 4, 5] or the reward as in [2, 3]:
    q(s, a) = 𝔼[r + γ(q(s', a') - τ ln(π(a|s)))]  # τ   treated as entropy scale
    q(s, a) = 𝔼[r/τ + γ(q(s', a') - ln(π(a|s)))]  # 1/τ treated as reward scale
and otherwise uses the "regular" Sarsa update target:
    q(s, a) = 𝔼[r + γq(s', a')]

### Implementation Details
This implementation of the RKL actor-critic algorithm allows for four different forms of
the RKL update. These are controlled by the constructor parameters
`scale_actor_lr_by_temperature` and `temperature_scales_entropy`. When
`temperature_scales_entropy`, then the temperature is treated as an entropy scale as in [1,
4, 5]. When `not temperature_scales_entropy`, then the temperature is treated as a reward
scale as in [2, 3]. The updates are as follows:

1. `scale_actor_lr_by_temperature and temperature_scales_entropy`:
    - Actor gradient = ∇𝔼[τ ln(π) - 𝔸]
    - Critic update target = r + γ(Q(s', a') - τ ln(π))

2. `not scale_actor_lr_by_temperature and temperature_scales_entropy`:
    - Actor gradient = ∇𝔼[ln(π) - 𝔸/τ]
    - Critic update target = r + γ(Q(s', a') - τ ln(π))

3. `scale_actor_lr_by_temperature and not temperature_scales_entropy`:
    - Actor gradient = ∇τ𝔼[ln(π) - 𝔸]
    - Critic update target = r/τ + γ(Q(s', a') - ln(π))

4. `not scale_actor_lr_by_temperature and not temperature_scales_entropy`:
    - Actor gradient = ∇𝔼[ln(π) - 𝔸]
    - Critic update target = r/τ + γ(Q(s', a') - ln(π))

where 𝔸 is the advantage function. All these updates are sensible but are implementation
details that can affect the algorithm's performance.  The original SAC update in [2, 3] is
equivalent to (4), and this is the implementation used in the original SAC codebase.
The StableBaselines3 codebase [4] and SpinningUp codebase [5] use the implementation in (1).

## References
[1] Greedification Operators for Policy Optimization: Investigating Forward
and Reverse KL Divergences. Chan, A., Silva H., Lim, S., Kozuno, T.,
Mahmood, A. R., White, M. In prepartion. 2021.

[2] Soft Actor-Critic: Off-Policy Maximum Entropy Deep Reinforcement
Learning with a Stochastic Actor. Haarnoja, T., Zhou, A., Abbeel, P.,
Levine, S. International Conference on Machine Learning. 2018.

[3] Soft Actor-Critic: Algorithms and Applications. Haarnoja, T.,
Zhou, A., Hartikainen, K., Tucker, G., Ha, S., Tan, J., Kumar, V.,
Zhu, H., Gupta, A., Abbeel, P., Levine, S. In preparation. 2019.

[4] Stable-Baselines3: Reliable Reinforcement Learning Implementations.
Raffin, A., Hill, A., Gleave, A., Kanervisto, A., Ernestus, M., Dormann,
N. Journal of Machine Learning Research. 2021.

[5] Spinning Up in Deep Reinforcement Learning. Achiam, J. 2018.
"""
mutable struct RKL{
        F<:AbstractFloat,           # Floating point type
        Π<:AbstractPolicy,          # Policy type
        ΠO,                         # Policy Optimizer
        V<:AbstractValueFunction,   # Value function type
        VO,                         # Value function optimizer
        E<:AbstractEnvironment,     # Environment type
        ER<:AbstractReplay,         # Experience replay
        R<:AbstractRNG,             # RNG
} <: AbstractAlgo
    _env::E
    _rng::R

    _π::Π
    _π_opt::ΠO
    _reparameterized::Bool

    _critic::V
    _critic_opt::VO
    _critic_target::V

    _soft_q::Bool
    _temperature_scales_entropy::Bool
    _scale_actor_lr_by_temperature::Bool

    _replay::ER
    _batch_size::Int

    _τ::F # Temperature
    _p::F # Polyak averaging constant
    _target_refresh_rate::Int
    _current_update::Int

    _steps_before_learning::Int
    _current_step::Int

    _num_samples::Int
    _baseline_actions::Int

    _is_training::Bool

    function RKL{F}(
        env::E,
        rng::R,
        π::Π,
        π_opt::ΠO,
        reparameterized,
        critic::V,
        critic_opt::VO,
        critic_target::V,
        soft_q,
        temperature_scales_entropy,
        scale_actor_lr_by_temperature,
        replay::ER,
        batch_size,
        τ,
        p,
        target_refresh_rate,
        steps_before_learning,
        num_samples,
        baseline_actions,
    ) where{
        F<:AbstractFloat,
        Π<:AbstractPolicy,
        ΠO,
        V<:AbstractValueFunction,
        VO,
        E<:AbstractEnvironment,
        ER<:AbstractReplay,
        R<:AbstractRNG,
    }
        # Ensure the policy and action space match
        if discrete(π) && continuous(action_space(env))
            error("cannot use discrete policy for continuous action space")
        elseif continuous(π) && discrete(action_space(env))
            error("cannot use continuous policy for discrete action space")
        end

        # Ensure the critic and action space match
        if discrete(critic) && continuous(action_space(env))
            error("cannot use discrete critic for continuous action space")
        elseif continuous(critic) && discrete(action_space(env))
            error("cannot use continuous critic for discrete action space")
        end

        if discrete(π) && num_samples > 0
            @warn "ignoring num_samples with discrete policy"
        end
        if num_samples < 1
            error("num_samples must be > 0")
        end

        if scale_actor_lr_by_temperature && τ == 0
            error("cannot scale actor stepsize by entropy scale of 0")
        end

        if discrete(π) && reparameterized
            error("cannot use the reparameterization trick with discrete actions")
        end

        if discrete(π) && baseline_actions > 0
            @warn "ignoring baseline_actions with discrete policy"
        end
        if baseline_actions < 0
            error("baseline_actions must be >= 0")
        end

        if batch_size < 0
            error("batch size must be > 0")
        end

        if target_refresh_rate <= 0
            error("expected target_refresh_rate > 0 but got $target_refresh_rate")
        end

        if steps_before_learning === nothing
            steps_before_learning = batch_size
        elseif steps_before_learning < 0
            error("expected steps_before_learning >= 0 but got $steps_before_learning")
        end

        return new{F,Π,ΠO,V,VO,E,ER,R}(
            env,
            rng,
            π,
            π_opt,
            reparameterized,
            critic,
            critic_opt,
            critic_target,
            soft_q,
            temperature_scales_entropy,
            scale_actor_lr_by_temperature,
            replay,
            batch_size,
            τ,
            p,
            target_refresh_rate,
            0,
            steps_before_learning,
            0,
            num_samples,
            baseline_actions,
            true,
        )
    end
end

function RKL{F}(
        env,
        rng;
        π,
        π_opt,
        critic,
        critic_opt,
        replay,
        batch_size::Int,
        τ,
        p,
        reparameterized::Bool = false,
        target_refresh_rate::Int = 1,
        steps_before_learning = nothing,
        num_samples::Int = 1,
        baseline_actions::Int = 0,
        soft_q::Bool = true,
        temperature_scales_entropy::Bool=true,
        scale_actor_lr_by_temperature::Bool = true,
) where {F<:AbstractFloat}
    critic_target = deepcopy(critic)

    return RKL{F}(
        env,
        rng,
        π,
        π_opt,
        reparameterized,
        critic,
        critic_opt,
        critic_target,
        soft_q,
        temperature_scales_entropy,
        scale_actor_lr_by_temperature,
        replay,
        batch_size,
        τ,
        p,
        target_refresh_rate,
        steps_before_learning,
        num_samples,
        baseline_actions,
    )
end

function RKL(
        env,
        rng;
        π,
        π_opt,
        critic,
        critic_opt,
        replay,
        batch_size::Int,
        τ,
        p,
        reparameterized::Bool = false,
        target_refresh_rate::Int = 1,
        steps_before_learning = nothing,
        num_samples::Int = 1,
        baseline_actions::Int = 0,
        soft_q::Bool = true,
        temperature_scales_entropy::Bool=true,
        scale_actor_lr_by_temperature::Bool = true,
)
    return RKL{Float32}(
        env,
        rng;
        π = π,
        π_opt = π_opt,
        critic = critic,
        critic_opt = critic_opt,
        replay = replay,
        batch_size = batch_size,
        τ = τ,
        p = p,
        reparameterized = reparameterized,
        target_refresh_rate = target_refresh_rate,
        steps_before_learning = steps_before_learning,
        num_samples = num_samples,
        baseline_actions = baseline_actions,
        soft_q = soft_q,
        temperature_scales_entropy = temperature_scales_entropy,
        scale_actor_lr_by_temperature = scale_actor_lr_by_temperature,
    )
end

RLCore.istrain(r::RKL) = r._is_training
RLCore.reset!(::RKL) = nothing

function RLCore.train!(r::RKL)
    trainmode!(r._π)
    trainmode!(r._critic)
    r._is_training = true
end

function RLCore.eval!(r::RKL)
    testmode!(r._π)
    testmode!(r._critic)
    r._is_training = false
end

function RLCore.select_action(r::RKL, state)
    r._current_step += 1
    if continuous(r._π)
        r._π(r._rng, state)

        return dropdims(r._π(r._rng, state); dims=2)
    else
        return r._π(r._rng, state)
    end
end

function RLCore.algostep!(r::RKL, state, action, reward, next_state, γ, done)::Nothing
    push!(r._replay, state, action, reward, next_state, γ, done)

    if r._current_step < r._steps_before_learning
        return
    end

    states, actions, rewards, next_states, γs, dones = rand(
        r._rng,
        r._replay,
        r._batch_size,
    )

    if ndims(actions) == 1
        actions = unsqueeze(actions; dims=1)
    end

    if r._batch_size == 1
        # Unsqueeze batch dimension
        rewards = [rewards]
        γs = [γs]
        states = unsqueeze(states; dims = ndims(states) + 1)
        actions = unsqueeze(actions; dims = ndims(actions) + 1)
        next_states = unsqueeze(next_states; dims = ndims(next_states) + 1)
    end

    r._current_update += 1

    _update_critic!(r, states, actions, rewards, next_states, γs)
    _update_actor!(r, states)

    return nothing
end

function _update_actor!(r::RKL, states)::Nothing
    if RLCore.continuous(action_space(r._env))
        _update_actor_continuous!(r, states)
    else
        _update_actor_discrete!(r, states)
    end

    return nothing
end

function _update_actor_continuous!(r::RKL, state_batch)::Nothing
    if r._reparameterized
        _update_actor_continuous_reparam!(r, state_batch)
    else
        _update_actor_continuous_ll!(r, state_batch)
    end

    return nothing
end

function _update_actor_continuous_reparam!(r::RKL, state_batch)::Nothing
    batch_size = r._batch_size
    state_size = size(observation_space(r._env))
    action_size = size(action_space(r._env))

    # Estimate the approximate state value for each state using v(s) = 𝔼[q(s, A)]
    if r._baseline_actions > 0
        v = _get_baseline(r, state_batch)
    end


    # Reparameterization trick loss
    gs = gradient(Flux.params(r._π)) do
        # Sample actions using reparameterized sampling
        actions = rsample(
            r._π, r._rng, state_batch;
            num_samples=r._num_samples
        )
        actions = reshape(actions, action_size..., :)

        # states = reshape(state_batch, state_size..., 1, batch_size)
        # states = repeat(states, ones(Int, length(state_size))..., r._num_samples, 1)
        # states = reshape(states, state_size..., :)
        states = repeat(
            state_batch;
            inner = (ones(Int, length(state_size))..., r._num_samples),
        )

        # Compute the advantage
        q = r._critic(states, actions)
        q = reshape(q, r._num_samples, batch_size)
        if r._baseline_actions > 0
            𝔸 = q .- v
        else
            𝔸 = q
        end

        # Compute the entropy for entropy regularization
        logπ = logprob(r._π, states, actions)

        # Compute the reparameterized loss
        if r._scale_actor_lr_by_temperature
            if r._temperature_scales_entropy
                    mean(r._τ .* logπ .- 𝔸)
            else
                    mean(logπ .- 𝔸) .* r._τ
            end
        else
            if r._temperature_scales_entropy
                    mean(logπ .- (𝔸 ./ r._τ))
            else
                    mean(logπ .- 𝔸)
            end
        end

    end

    Flux.Optimise.update!(r._π_opt, Flux.params(r._π), gs)

    return nothing
end

function _update_actor_continuous_ll!(r::RKL, state_batch)::Nothing
    batch_size = r._batch_size
    state_size = size(observation_space(r._env))
    action_size = size(action_space(r._env))

    # Sample actions for each state in the batch, needed to estimate the gradient in each
    # state: ∇J = 𝔼[q(S, A)/τ - ln(π(A∣S)]
    actions = rand(r._rng, r._π, state_batch; num_samples=r._num_samples)
    actions = reshape(actions, action_size..., :)

    # Repeat states for each action sample in that state
    states = repeat(
        state_batch;
        inner = (ones(Int, length(state_size))..., r._num_samples),
    )

    q = r._critic(states, actions)

    q = reshape(q, r._num_samples, batch_size)

    if r._baseline_actions > 0
        v = _get_baseline(r, state_batch)
        𝔸 = q .- v
    else
        𝔸 = q
    end

    # Compute the gradient of the RKL objective
    gs = gradient(Flux.params(r._π)) do

        logπ = logprob(r._π, states, actions)
        logπ = reshape(logπ, r._num_samples, batch_size)

        scale = if r._τ != 0
            # Soft RKL
            if r._scale_actor_lr_by_temperature
                if r._temperature_scales_entropy
                    @. 𝔸 - r._τ * logπ
                else
                    @. (𝔸 - logπ) * r._τ
                end
            else
                if r._temperature_scales_entropy
                    @. 𝔸 / (r._τ) - logπ
                else
                    @. 𝔸 - logπ
                end
            end

        else
            # Hard RKL
            𝔸
        end
        scale = Zygote.dropgrad(scale)
        # @time scale = ignore_derivatives(scale)

        -mean(logπ .* scale)
    end

    Flux.Optimise.update!(r._π_opt, Flux.params(r._π), gs)

    return nothing
end

function _update_actor_discrete!(r::RKL, state_batch)::Nothing
    if !(action_space(r._env) isa Spaces.Discrete)
        error("unknown action space type $(typeof(action_space(r._env)))")
    end

    q = r._critic(state_batch)

    gs = gradient(Flux.params(r._π)) do

        logπ = logprob(r._π, state_batch)

        scale = if r._τ != zero(r._τ)
            if !r._scale_actor_lr_by_temperature
                @. (q / r._τ) - logπ
            else
                @. q - (r._τ * logπ)
            end
        else
            q
        end
        scale = Zygote.dropgrad(scale)

        prob = exp.(logπ)

        loss = prob .* scale
        loss = sum(loss; dims=1)
        -mean(loss)
    end

    Flux.Optimise.update!(r._π_opt, Flux.params(r._π), gs)

    return nothing
end

function _update_critic!(r::RKL, states, actions, rewards, next_states, γs)::Nothing
    # Calculate the q-values of actions (drawn from the current policy) in the next states
    if continuous(r._π)
        next_actions = dropdims(r._π(r._rng, next_states); dims=2)
    else
        next_actions = r._π(r._rng, next_states)
    end
    next_q_values = r._critic_target(next_states, next_actions)

    # Adjust the next q values if using soft action-value functions
    if r._soft_q
        logπ = logprob(r._π, next_states, next_actions)

        if r._temperature_scales_entropy
            # [1, 4, 5] do this
            next_q_values .-= r._τ .* logπ
        else
            # [2, 3] do this
            next_q_values ./= r._τ
            next_q_values .-= logπ
        end
    end

    # Update the critic
    gs = gradient(Flux.params(r._critic)) do
        loss(r._critic, states, actions, rewards, γs, next_q_values)
    end
    Flux.Optimise.update!(r._critic_opt, Flux.params(r._critic), gs)

    # Update the target critic
    if (r._current_update % r._target_refresh_rate) == 0
        polyak!(r._p, r._critic_target, r._critic)
    end

    return nothing
end

function _get_baseline(r::RKL{F}, state_batch)::AbstractArray{F} where {F<:AbstractFloat}
    if r._baseline_actions > 0
        batch_size = r._batch_size
        state_size = size(observation_space(r._env))
        action_size = size(action_space(r._env))

        actions = rand(r._rng, r._π, state_batch; num_samples=r._baseline_actions)
        actions = reshape(actions, action_size..., :)

        states = repeat(
            state_batch;
            inner = (ones(Int, length(state_size))..., r._baseline_actions),
        )

        q = r._critic(states, actions)
        q = reshape(q, r._baseline_actions, batch_size)

        v = mean(q; dims = 1)
    else
        error("cannot compute baseline with 0 actions")
    end
end

function Base.show(io::IO, r::RKL)
    print(io, "RKL")
end
