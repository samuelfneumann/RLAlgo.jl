# Right now, there is a bit of a limitation in that each network in an n-Q net must be
# separate and share no hidden layers. This could easily be changed be getting the user to
# input a single network that outputted an N-tuple of values. Then, to determine how many
# N's in the n-Q network, we just do length(input_net(sample(observation_space(env))),
# sample(action_space(env))).
# Then, the user can use separate networks by inputting a Split(Chain, Chain, ...) for
# example, or they could use a network that shared hidden layers, but outputted N
# predictions.
#
# Actually, this would be possible I think with just a new constructor!

# Provides the function approximator with one input: state. FA should output one
# action-value for each action


struct DiscreteQ{T} <: AbstractValueFunction
    _approximators::T
    _n_approximators::Int
    _loss::Function
    _reduct::Function
end

function DiscreteQ(approximators::Vector, loss=Flux.Losses.mse, reduct=minimum)
    n_approx = length(approximators)
    return DiscreteQ(Split(approximators, true), n_approx, loss, reduct)
end

Flux.@functor DiscreteQ
Flux.trainable(q::DiscreteQ) = (q._approximators,)
RLCore.continuous(::DiscreteQ) = false
RLCore.discrete(::DiscreteQ) = true

function RLCore.n_approximators(q::DiscreteQ)::Int
    return q._n_approximators
end

function (q::DiscreteQ)(state; reduct=true)
    out = q._approximators(state)

    if reduct || n_approximators(q) == 1
        last = length(size(out))
        out = dropdims(q._reduct(out; dims=last); dims=last)
    end

    return out
end

function (q::DiscreteQ)(state, action; reduct=true)
    values = q(state; reduct=reduct)
    if reduct
        batch_size = size(values)[end]
        return values[[CartesianIndex(action[i], i) for i in 1:batch_size]]
    else
        if n_approximators(q) == 1
            batch_size = size(values)[end]
            indices = [
                CartesianIndex(action[i], i)
                for i in 1:batch_size
            ]
            return values[indices]
        else
            batch_size = size(values)[end-1]
            return reshape(
                values[[
                    CartesianIndex(action[i], i, j)
                    for j in 1:n_approximators(q), i in 1:batch_size
                ]],
                size(values)[2:end],
            )
        end
    end
end

function RLCore.loss(q::DiscreteQ, state, action, reward, γ, next_q)
    target = dropgrad(reward + γ .* next_q)
    q_vals = q(state, action, reduct=false)

    if n_approximators(q) > 1
        target = repeat(target, 1, n_approximators(q))
        return q._loss(target, q_vals)
    else
        return q._loss(target, q_vals)
    end
end

function RLCore.loss(q::DiscreteQ, state, action, reward, γ, next_state, next_action)
    next_q = q(next_state, next_action; reduct=true)
    return loss(q, state, action, reward, γ, next_q)
end

####################################################################

# Provides the function approximator with a tuple of inputs: (state, action)
struct Q{T} <: AbstractValueFunction
    _approximators::T
    _n_approximators::Int
    _loss::Function
    _reduct::Function
end

# approximators should be a Vector of networks
function Q(approximators::Vector, loss=Flux.Losses.mse, reduct=minimum)
    n_approx = length(approximators)
    return Q(Split(approximators, true), n_approx, loss, reduct)
end

Flux.@functor Q
Flux.trainable(q::Q) = (q._approximators,)
RLCore.continuous(::Q) = true
RLCore.discrete(::Q) = false

function RLCore.n_approximators(q::Q)::Int
    return q._n_approximators
end

function (q::Q)(state_action::Tuple{AbstractArray{T}}; reduct=True) where {T}
    if length(state_action) != 2
        len = length(state_action)
        error("expected input to have length 2 (state, action) but got $(len)")
    end
    return q(input[1], input[2]; reduct=reduct)
end

function (q::Q)(state, action; reduct=true)
    out = q._approximators((state, action))

    if size(out)[1] != 1
        # Ensure the function approximators only output a single action-value
        size(out)[1]
        error("expected network to output one q-value but got $(size)")
    end

    if n_approximators(q) == 1
        # Squeeze number of approximator dimension
        last = length(size(out))
        out = dropdims(out; dims=last)
    elseif reduct
        # Reduce over the predictions from each approximator
        last = length(size(out))
        out = dropdims(q._reduct(out; dims=last); dims=last)
    end

    # Squeeze the number of action-value predictions, which is always 1
    return dropdims(out, dims=1)
end

function RLCore.loss(q::Q, state, action, reward, γ, next_q)
    target = dropgrad(reward .+ γ .* next_q)
    q_vals = q(state, action, reduct=false)

    if n_approximators(q) > 1
        # target = repeat(target, 1, n_approximators(q)) # This could be expensive
        return sum(mean(q._loss.(target, q_vals); dims=1))
    else
        return q._loss(target, q_vals)
    end
end

function RLCore.loss(q::Q, state, action, reward, γ, next_state, next_action)
    next_q = q(next_state, next_action; reduct=true)
    return loss(q, state, action, reward, γ, next_q)
end
