# TODO: modify to work with new RLCore interface

mutable struct Sarsa{F<:AbstractFloat,T,R<:AbstractRNG} <: AbstractAlgo
    # Fields having to do with SARSA update
    weights::AbstractMatrix{F}

    α::F  # stepsize
    ε::F
    _rng::R

    λ::F # Decay rate
    trace_type::Symbol
    trace::AbstractMatrix{F}

    use_tile_coding::Bool
    _tiler::T
end

function Sarsa{F}(
        env::AbstractEnvironment,
        rng::AbstractRNG;
        α=1f0,
        ε=0.05f0,
        λ=0f0,
        trace_type = :none,
        tiler=nothing,
) where {F}
    if env |> action_space |> continuous
        error("sarsa must use discrete actions")
    end

    action_dims = env |> action_space |> size
    if length(action_dims) != 1 || (length(action_dims) == 1 && action_dims[1] != 1)
        error("sarsa must use a single action")
    end

    if λ > zero(λ) && trace_type === :none
        error("cannot use decay without traces")
    end

    num_actions = env |> action_space |> high

    use_tiler = (tiler !== nothing)
    if !use_tiler
        observation_dims = env |> observation_space |> size
    else
        observation_dims = features(tiler)
        α /= nonzero(tiler)
    end

    weights = zeros(F, observation_dims[1], num_actions[1])
    trace = zeros(F, observation_dims[1], num_actions[1])

    return Sarsa{F}(
        weights,
        α,
        ε,
        rng,
        λ,
        trace_type,
        trace,
        use_tiler,
        tiler,
    )
end

_update_trace!(s::Sarsa, state, action, ::Val{:none}) = nothing

function _update_trace!(s::Sarsa{F}, state, action, ::Val{:accumulating}) where {F}
    s.trace[state, action] .+= one(F)
end

function _update_trace!(s::Sarsa{F}, state, action, ::Val{:replacing}) where {F}
    s.trace[state, action] .= one(F)
end

function RLCore.step!(
        s::Sarsa{F,TileCoder},
        state,
        action,
        reward,
        next_state,
        γ,
        done,
) where {F}
    state = index(s._tiler, state)

    δ = reward
    δ -= sum(s.weights[state, action])

    _update_trace!(s, state, action, s.trace_type)

    if γ != 0
        next_action = _ε_greedy_sample(s, next_state)[1]
        next_state = index(s._tiler, next_state)
        δ += (γ * sum(s.weights[next_state, next_action]))
    end

    # Update the weights
    if s.trace_type !== :none
        s.weights .+= (s.α * δ .* s.trace)
        s.trace .*= (γ * s.λ)
    else
        s.weights[state, s._action] .+= (s.α * δ)
    end

    return nothing
end

function RLCore.step!(s::Sarsa{F}) where {F}
    δ = s._reward - s.weights[:, s._action] ⋅ s._state

    if s._γ != 0
        s._next_action = _ε_greedy_sample(s, s._next_state)[1]
        δ += s.weights[:, s._next_action] ⋅ s._next_state
    end

    if s.use_traces
        s.weights .+= (s.α * δ .* s.trace)
        s.trace .*= (s._γ * s.λ)
    else
        s.weights[:, s._action] .+= (s.α * δ .* s._state)
    end

    return nothing
end

function RLCore.select_action(s::Sarsa, state::Vector{T})::Vector{Int} where {T<:Number}
    if s._first_action
        s._first_action = false
        s._action = s._next_action = _ε_greedy_sample(s, state)
        return [s._action]

    elseif !isequal(state, s._next_state)
        println("warning: input state was not used as the next state in the " *
            "SARSA update to select the next action. Sampling a new action.")
        return [_ε_greedy_sample(s, state)]

    end

    return [s._next_action]
end

function _ε_greedy_sample(s::Sarsa, state::Vector{T})::Int where {T<:Number}
    # Choose random action with probability ε
    if s.ε > 0 && rand(s._rng, Float32) < s.ε
        action = abs(rand(s._rng, Int)) % size(s.weights)[2] + 1
    else
        # Compute all the action values in the given state, flatten into a vector
        if s.use_tile_coding
            state = index(s._tiler, state)
            values = [sum(s.weights[state, :]; dims=1)...]
        else
            values = [(state' * s.weights)...]
        end

        # With 1-ε probability, choose the action with the maximal value
        max = maximum(values)
        max_actions = findall(x -> x == max, values)

        # If multiple highest valued actions, choose one randomly
        if length(max_actions) > 1
            max_actions = shuffle(s._rng, max_actions)
        end
        action = max_actions[1]
    end

    return action
end

function RLCore.end_episode!(s::Sarsa)
    s.trace .= 0
    s._first_action = true
    s._action = s._next_action = -1
    s._state = s._next_state = nothing

    return nothing
end
