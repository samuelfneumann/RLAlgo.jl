"""
    InitialPolicy{Π,A,E,R} <: AbstractAlgoActionWrapper

Use a different policy of type `Π` for the first `N` steps of agent-environment interaction
for agent with type `A`, environment of type `E` and RNG of type `R`.
"""
mutable struct InitialPolicy{
        Π<:AbstractPolicy,
        A<:AbstractAlgo,
        E<:AbstractEnvironment,
        R<:AbstractRNG,
} <: AbstractAlgoActionWrapper
    _current_steps::Int
    _π::Π
    _algo::A
    _env::E
    _rng::R
    _max_steps::Int

    function InitialPolicy{Π,A,E,R}(π, algo, env, rng, steps) where {Π,A,E,R}
        return new{Π,A,E,R}(0, π, algo, env, rng, steps)
    end
end

function InitialPolicy(π::Π, algo::A, env::E, rng::R, steps) where {Π,A,E,R}
    return InitialPolicy{Π,A,E,R}(π, algo, env, rng, steps)
end

function RLCore.action(e::InitialPolicy, state, orig_action)
    if e._current_steps <= e._max_steps
        e._current_steps += 1

        if continuous(action_space(e._env))
            act = e._π(e._rng, state)
            if ndims(act) == 2
                # Drop the batch dimension if it is included, which it generally is
                return dropdims(act; dims=2)
            else
                return act
            end
        else
            return e._π(e._rng, state)
        end

    else
        # Done the initial exploration phase, so return the original action
        return orig_action
    end
end

RLCore.wrapped(e::InitialPolicy) = e._algo
