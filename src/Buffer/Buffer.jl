module Buffer

export AbstractReplay, ExperienceReplay

using RLCore
using Random

include("CircularBuffer.jl")
include("SumTree.jl")

import Random
import DataStructures

# ####################################
# Abstract Replay Buffer
# ####################################
abstract type AbstractReplay end

proc_state(er::AbstractReplay, x) = identity(x)

Base.keys(er::AbstractReplay) = 1:length(er)

function Base.iterate(asr::AbstractReplay)
    state = 1
    result = asr[state]
    state += 1
    (result, state)
end

function Base.iterate(asr::AbstractReplay, state::Integer)
    if state > length(asr)
        return nothing
    end

    result = asr[state]
    state += 1

    (result, state)
end

# ####################################
# Experience Replay Buffer
# ####################################
mutable struct ExperienceReplay{CB<:CircularBuffer} <: AbstractReplay
    buffer::CB
end

function RLCore.reset!(er::ExperienceReplay)
    reset!(er.buffer)
end

ExperienceReplay(capacity, types, shapes, column_names) = begin
    cb = CircularBuffer(capacity, types, shapes, column_names)
    ExperienceReplay(cb)
end

function ExperienceReplay(env::AbstractEnvironment, capacity::Int; F = Float32)
    obs_size = size(observation_space(env))
    act_size = size(action_space(env))
    r_size = γ_size = done_size = 1

    obs_type = eltype(observation_space(env))
    act_type = eltype(action_space(env))
    if F === nothing
        F = typeof(γ(env))
    end

    types = (obs_type, act_type, F, obs_type, F, Bool)
    shapes = (obs_size, act_size, r_size, obs_size, γ_size, done_size)
    names = (:s, :a, :r, :ns, :γ, :done)

    return ExperienceReplay(
        capacity,
        types,
        shapes,
        names,
    )
end

Base.length(er::ExperienceReplay) = length(er.buffer)
Base.getindex(er::ExperienceReplay, idx) = er.buffer[idx]
Base.view(er::ExperienceReplay, idx) = @view er.buffer[idx]

Base.push!(er::ExperienceReplay, experience) = push!(er.buffer, experience)
Base.push!(er::ExperienceReplay, experience...) = push!(er.buffer, experience)

function Random.rand(er::ExperienceReplay, batch_size::Int)
    Random.rand(Random.GLOBAL_RNG, er, batch_size)
end

function Random.rand(rng::Random.AbstractRNG, er::ExperienceReplay, batch_size::Int)
    idx = rand(rng, 1:length(er), batch_size)
    return er[idx]
end

# ####################################
# Sequence Replay Buffer
# ####################################
abstract type AbstractSequenceReplay <: AbstractReplay end

mutable struct SequenceReplay{CB} <: AbstractSequenceReplay
    buffer::CB
    place::Int64
end

function SequenceReplay(size, types, shapes, column_names)
    cb = CircularBuffer(size, types, shapes, column_names)
    SequenceReplay(cb, 1)
end

function RLCore.reset!(er::SequenceReplay)
    reset!(er.buffer)
    er.place = 1
end

Base.length(er::SequenceReplay) = length(er.buffer)
Base.getindex(er::SequenceReplay, idx) =
    er.buffer[idx]

Base.view(er::SequenceReplay, idx) =
    @view er.buffer[idx]

Base.push!(er::SequenceReplay, experience...) = push!(er, experience)

function Base.push!(er::SequenceReplay, experience)
    if er.buffer._full
        er.place = (er.place % capacity(er.buffer)) + 1
    end
    push!(er.buffer, experience)
end

function Random.rand(er::SequenceReplay, batch_size, seq_length)
    Random.rand(Random.GLOBAL_RNG, er, batch_size, seq_length)
end

function Random.rand(rng::Random.AbstractRNG, er::SequenceReplay, batch_size, seq_length)
    start_inx = rand(rng, 1:(length(er) + 1 - seq_length), batch_size)
    e = [view(er, start_inx .+ (i-1)) for i ∈ 1:seq_length]
    start_inx, e
end

mutable struct EpisodicSequenceReplay{CB} <: AbstractSequenceReplay
    buffer::CB
    place::Int64
    terminal_locs::Vector{Int}
    terminal_symbol::Symbol
end

function EpisodicSequenceReplay(size, types, shapes, column_names; terminal_symbol = :t)
    cb = CircularBuffer(size, types, shapes, column_names)
    EpisodicSequenceReplay(cb, 1, Int[], terminal_symbol)
end

function RLCore.reset!(er::EpisodicSequenceReplay)
    reset!(buffer)
    er.place = 1
    er.terminal_locs = Int[]
end

Base.length(er::EpisodicSequenceReplay) = length(er.buffer)
Base.getindex(er::EpisodicSequenceReplay, idx) =
    er.buffer[idx]

Base.view(er::EpisodicSequenceReplay, idx) =
    @view er.buffer[idx]

function Base.push!(er::EpisodicSequenceReplay, experience)
    if er.buffer._full
        er.place = (er.place % capacity(er.buffer)) + 1
    end
    push!(er.buffer, experience)
end

function get_episode_ends(er::EpisodicSequenceReplay)
    # TODO: n-computations. Maybe store in a cache?
    findall((exp)->exp::Bool, er.buffer._stg_tuple[er.terminal_symbol])
end

function get_valid_starting_range(s, e, seq_length)
    if e - seq_length <= s
        s:s
    else
        (s:e-seq_length)
    end
end

function get_valid_indicies(er::EpisodicSequenceReplay, seq_length)
    # episode_ends = get_episode_ends(er)
    1:(length(er) + 1 - seq_length)
end

function get_sequence(er::EpisodicSequenceReplay, start_ind, max_seq_length)
    # ret = [view(er, start_ind)]
    ret = [er[start_ind]]
    er_size = length(er)
    if ((start_ind + 1 - 1) % er_size) + 1 == er.place ||
        ret[end][er.terminal_symbol][]::Bool
        return ret
    end

    for i ∈ 1:(max_seq_length-1)
        push!(ret, er[(((start_ind + i - 1) % er_size) + 1)])
        if ret[end][er.terminal_symbol][]::Bool || ((start_ind + i + 1 - 1) % er_size) + 1 == er.place
            break
        end
    end

    return ret
end

function Random.rand(er::EpisodicSequenceReplay, batch_size, max_seq_length)
   Random.rand(Random.GLOBAL_RNG, er, batch_size, max_seq_length)
end

function Random.rand(
        rng::Random.AbstractRNG,
        er::EpisodicSequenceReplay,
        batch_size,
        max_seq_length,
)
    # get valid starting indicies
    valid_inx = get_valid_indicies(er, 1)
    start_inx = rand(rng, valid_inx, batch_size)
    exp = [get_sequence(er, si, max_seq_length) for si ∈ start_inx]
    start_inx, exp
    # padding and batching handled by agent.
end

# ####################################
# Experience Replay Buffer for Images
# ####################################
include("ImageReplay.jl")

end
