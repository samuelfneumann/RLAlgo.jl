module RLAlgo

# Buffer module
include("Buffer/Buffer.jl")

# Utilities module
include("Util/Util.jl")

# Algorithms using linear function approximation
module Linear

export Sarsa
export TraceType, AccumulatingTrace, ReplacingTrace
@enum TraceType AccumulatingTrace=1 ReplacingTrace=2

using RLCore
using RLReps
using Random

# TODO: Update Sarsa to work with new RLCore interfaces
# include("Algo/Linear/Sarsa.jl")

end

module Wrapper
export InitialPolicy

using RLCore
using Random

include("Algo/Wrapper/InitialPolicy.jl")
end

# Algorithms using Flux for deep neural network function approximation
module Deep

export
    # Algos
    RKL, FKL, CCEM,

    # Value Functions
    Q, DiscreteQ,

    # Unbounded or "Half-Bounded" Policies
    IIDPolicy,
    Policy,
    NormalPolicy,
    LaplacePolicy,
    GammaPolicy,
    LogisticPolicy,

    # Truncated policies
    TruncatedPolicy,
    TruncatedNormalPolicy,
    TruncatedLaplacePolicy,

    # Bounded policies
    BoundedPolicy,
    BetaPolicy,
    KumaraswamyPolicy,
    LogitNormalPolicy,
    ArctanhNormalPolicy,

    StaticUniform,

    SoftmaxPolicy

using ..Util.FluxUtil
using ..Buffer
using RLCore
using Random
using RLDist
using Flux
using Zygote
using MLUtils
using LinearAlgebra: Transpose
# using ChainRules: @ignore_derivatives

using RLEnv.Environment

include("Algo/Deep/ValueFunction/ValueFunction.jl")
include("Algo/Deep/Policy/IIDPolicy.jl")
include("Algo/Deep/Policy/SoftmaxPolicy.jl")
include("Algo/Deep/Policy/BoundedPolicy.jl")
include("Algo/Deep/Policy/UnBoundedPolicy.jl")
include("Algo/Deep/Policy/TruncatedPolicy.jl")
include("Algo/Deep/Policy/StaticPolicy.jl")
include("Algo/Deep/RKL.jl")
include("Algo/Deep/FKL.jl")
include("Algo/Deep/CCEM.jl")

# function run()
#     net = Split(Dense(2, 1, softplus), Dense(2, 1, exp))
#     env = Pendulum(Xoshiro(1); continuous=true)
#     b = Beta(env, net)

#     println(logprob(b, Float32[[1 1]; [2 2]], Float32[1.5 ;; -0.2]))
#     println(logprob(b, Float32[1, 1], Float32[1.5]))

#     println(size(RLCore.sample(b, Float32[1, 1])))
#     println(size(RLCore.sample(b, Float32[1, 1]; num_samples=10)))

#     println(size(RLCore.sample(b, Float32[[1 1]; [2 2]])))
#     println(size(RLCore.sample(b, Float32[[1 1]; [2 2]]; num_samples=10)))
# end
end

end # module RLAlgo
