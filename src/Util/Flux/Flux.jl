module FluxUtil

export Split, Concat
export polyak!, set!

using Flux
using MLUtils

include("Functions.jl")
include("Layers.jl")
end
