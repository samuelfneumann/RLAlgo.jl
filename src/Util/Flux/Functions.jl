using LinearAlgebra

"""
    polyak!(β::AbstractFloat, dest, src)

Compute the polyak average of the weights of src with the weights of dest, storing the
result as the weights of dest.
"""
function polyak!(β::T, dest, src) where {T<:AbstractFloat}
	if β != 1.0
		for i = 1:length(Flux.params(dest))
            axpby!(
                β,
                Flux.params(src)[i],
                oneunit(T) - β,
                Flux.params(dest)[i],
            )
		end
	else
		for i = 1:length(Flux.params(dest))
            set!(Flux.params(dest)[i], Flux.params(src)[i])
		end
	end
end

"""
    set!(dest, src)

Set the weights of dest to equal the weights of src.
"""
function set!(dest, src)
    for i = 1:length(Flux.params(dest))
        Flux.params(dest)[i] .= Flux.params(src)[i]
    end
end
