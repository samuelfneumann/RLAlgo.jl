# #################
# Custom layers
# #################
struct Split{T}
  paths::T
  stack::Bool
end

Split(paths...) = Split(paths, false)

Flux.@functor Split
Flux.trainable(s::Split) = (s.paths...,)

function (m::Split)(x)
    out = map(f -> f(x), m.paths)

    if m.stack
        last = length(size(out[1])) + 1
        out = stack(out; dims=last)
    end

    return out
end

struct Concat
  dims::Int
end

Flux.@functor Concat
Flux.trainable(::Concat) = tuple()

function (c::Concat)(args)
    if c.dims == 1
        return reduce(vcat, args)
    elseif c.dims == 2
        return reduce(hcat, args)
    else
        return cat(args...; dims=c.dims)
    end
end


